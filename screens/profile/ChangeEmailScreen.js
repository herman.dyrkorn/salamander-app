import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  KeyboardAvoidingView,
  SafeAreaView,
} from "react-native";
import { connect } from "react-redux";
import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import AccountData from "../../components/AccountData";
import CustomButton from "../../components/CustomButton";
import { TextInput, HelperText } from "react-native-paper";
import { onEditEmail } from "../../redux";
import {
  EMAIL_REGEX,
  INVALID_EMAIL_MESSAGE,
  NAME_MAX_LENGTH,
} from "../../constants/inputRequirements";
import {
  toastSuccess, 
  toastError,
  toast500
} from "../../constants/toasts"; 

/**
 * This component renders the screen where a user can change their email. To do this, the user has to re-authenicate due to it being a critical 
 * feature. 
 * 
 * @param {*} props - used for redux and navigation. 
 * 
 * @returns the component. 
 */
const _ChangeEmailScreen = (props) => {
  const { userReducer, onEditEmail } = props;
  const [email, setEmail] = useState("");
  const [emailHint, setEmailHint] = useState(false);
  const [showIndicator, setShowIndicator] = useState(false);

  const { user } = userReducer;

  // To make the back-button on android behave correctly and not go to the VerifyPasswordScreen
  useEffect(() => {
    props.navigation.addListener('beforeRemove', (e) => {
      e.preventDefault();
      props.navigation.navigate("Profile");
    });
  }, []);
  
  const checkEmail = (email) => {
    setEmail(email);
    setEmailHint(!email.match(EMAIL_REGEX) && email !== "");
  };

  const editEmail = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("newEmail", email);

    setShowIndicator(true);

    APIKit.put("/user", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          onEditEmail({ email: email });
          props.navigation.navigate("Profile");
          toastSuccess("top", 2000, response.data.message); 
        } else {
          toastError("top", 1000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <KeyboardAvoidingView behavior="padding">
        <SafeAreaView style={{ flex: 1, alignItems: "stretch" }}>
          <CustomActivityIndicator
            style={styles.activityIndicator}
            visible={showIndicator}
          />
          <AccountData
            currentLabel="Current Email"
            current={user.email}
            newLabel="New Email"
          />
          <View style={styles.textfield}>
            <TextInput
              mode="flat"
              label="Email"
              placeholder="Email"
              type="email"
              keyboardType="email-address"
              autoCapitalize="none"
              autoFocus={true}
              onChangeText={(email) => checkEmail(email)}
              value={email}
              onSubmitEditing={editEmail}
              maxLength={NAME_MAX_LENGTH}
            />
            {emailHint && (
              <HelperText type="error" visible={emailHint}>
                {INVALID_EMAIL_MESSAGE}
              </HelperText>
            )}
          </View>
          <CustomButton
            style={styles.button}
            mode="contained"
            title="Confirm"
            onPress={() => editEmail()}
            disabled={!(!emailHint && email !== "")}
          />
          <CustomButton
            style={styles.button}
            mode="outlined"
            title="Cancel"
            onPress={() => props.navigation.navigate("Profile")}
          />
        </SafeAreaView>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const ChangeEmailScreen = connect(mapStateToProps, { onEditEmail })(
  _ChangeEmailScreen
);

export { ChangeEmailScreen };

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  button: {
    width: "40%",
    alignSelf: "center",
    marginTop: 20,
  },
  textfield: {
    marginTop: 5,
    width: "80%",
    alignSelf: "center",
  },
});
