import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { Divider } from "react-native-paper";
import { connect } from "react-redux";

import CustomTouchableOpacity from "../../components/CustomTouchableOpacity";
import { AuthContext } from "../../constants/context";
import { onUserSignOut } from "../../redux/actions/UserActions";

/**
 * This component renders the profile screen where the user can access all features regarding data in the system. Some features are
 * restricted to role, and some featuers require re-authentication to get access. The user can also log out of the application through
 * this screen. 
 * 
 * @param {*} props - used for redux and navigation. 
 * 
 * @returns the component. 
 */
const _ProfileScreen = (props) => {
  const { userReducer, onUserSignOut } = props;

  const { user } = userReducer;

  const { signOutApp } = React.useContext(AuthContext);

  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.text}>Personal Data</Text>
        <Divider />
        <CustomTouchableOpacity
          title="Name"
          placeholder={user.name}
          icon="chevron-right"
          onPress={() => props.navigation.navigate("ChangeName")}
        />
        <Divider />
        <CustomTouchableOpacity
          title="Email"
          placeholder={user.email}
          icon="chevron-right"
          onPress={() => {
            props.navigation.navigate("Verify", {
              screen: "VerifyPassword",
              params: { nextNavigation: "ChangeEmail" },
            });
          }}
        />
        <Divider />
        <CustomTouchableOpacity
          title="Password"
          icon="chevron-right"
          onPress={() => {
            props.navigation.navigate("Verify", {
              screen: "VerifyPassword",
              params: { nextNavigation: "ChangePassword" },
            });
          }}
        />
        <Divider />
        {user.admin && (
          <View>
            <Text style={styles.text}>Administrate</Text>
            <Divider />
            <CustomTouchableOpacity
              title="Pending Users"
              icon="chevron-right"
              onPress={() => {
                props.navigation.navigate("Verify", {
                  screen: "VerifyPassword",
                  params: { nextNavigation: "PendingUsers" },
                });
              }}
            />
            <Divider />
            <CustomTouchableOpacity
              title="Manage Users"
              icon="chevron-right"
              onPress={() => {
                props.navigation.navigate("Verify", {
                  screen: "VerifyPassword",
                  params: { nextNavigation: "ManageUsers" },
                });
              }}
            />
            <Divider />
            <CustomTouchableOpacity
              title="Manage Salamanders"
              icon="chevron-right"
              onPress={() => {
                props.navigation.navigate("Verify", {
                  screen: "ManageSalamanders",
                });
              }}
            />
            <Divider />
          </View>
        )}
        <Text style={styles.text}>Account Management</Text>
        <Divider />
        <CustomTouchableOpacity
          title="Delete Account"
          onPress={() => {
            props.navigation.navigate("Verify", {
              screen: "VerifyPassword",
              params: { nextNavigation: "DeleteAccount" },
            });
          }}
        />
        <Divider />
        <View style={styles.signOut}>
          <Divider />
          <CustomTouchableOpacity
            style={styles.signOutPlaceholder}
            placeholder="Sign Out"
            onPress={() => onUserSignOut({ signOutApp })}
          />
          <Divider />
        </View>
      </View>
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const ProfileScreen = connect(mapStateToProps, { onUserSignOut })(
  _ProfileScreen
);

export { ProfileScreen };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
  },
  text: {
    paddingTop: 25,
    paddingStart: 10,
    paddingBottom: 10,
    fontSize: 18,
  },
  signOut: {
    paddingTop: 20,
    marginBottom: 20,
  },
  signOutPlaceholder: {
    paddingStart: 0,
    color: "#435768",
    fontWeight: "bold",
    fontSize: 16,
    marginLeft: 0,
  },
});
