import React, { useState } from "react";
import {
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  SafeAreaView,
} from "react-native";
import AccountData from "../../components/AccountData";
import { connect } from "react-redux";
import CustomButton from "../../components/CustomButton";
import { TextInput } from "react-native-paper";
import { onEditName } from "../../redux";
import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import {
  NAME_MAX_LENGTH,
} from "../../constants/inputRequirements";
import {
  toastSuccess, 
  toastError,
  toast500
} from "../../constants/toasts"; 

/**
 * This component renders the screen where a user can change their name in the system. The name is simply for aesthetic reasons, and is not used for 
 * anything other than being shown to the user. Can be used in future releases. 
 * 
 * @param {*} props - used for redux and navigation. 
 * 
 * @returns the component. 
 */
const _ChangeNameScreen = (props) => {
  const { userReducer, onEditName } = props;

  const { user } = userReducer;

  const [name, setName] = useState("");
  const [showIndicator, setShowIndicator] = useState(false);

  const editName = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("newName", name);

    setShowIndicator(true);

    APIKit.put("/user", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          onEditName({ name: name });
          props.navigation.navigate("Profile");
          toastSuccess("top", 2000, response.data.message); 
        } else {
          toastError("top", 1000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <KeyboardAvoidingView behavior="padding">
        <SafeAreaView style={{ flex: 1, alignItems: "stretch" }}>
          <CustomActivityIndicator style={styles.activityIndicator} visible={showIndicator} />
          <AccountData
            currentLabel="Current Name"
            current={user.name}
            newLabel="New Name"
          />
          <TextInput
            style={styles.textfield}
            mode="flat"
            label="Name"
            autoCapitalize="words"
            placeholder="Name"
            onChangeText={(name) => setName(name)}
            value={name}
            maxLength={NAME_MAX_LENGTH}
          />
          <CustomButton
            style={styles.button}
            title="Confirm"
            mode="contained"
            onPress={() => editName()}
          />
        </SafeAreaView>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const ChangeNameScreen = connect(mapStateToProps, { onEditName })(
  _ChangeNameScreen
);

export { ChangeNameScreen };

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  button: {
    width: "40%",
    alignSelf: "center",
    marginTop: 20,
  },
  textfield: {
    marginTop: 5,
    width: "80%",
    alignSelf: "center",
  },
});
