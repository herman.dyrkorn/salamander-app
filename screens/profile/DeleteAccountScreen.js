import React, { useState } from "react";
import { StyleSheet, Text, View, ScrollView, Alert } from "react-native";
import CustomButton from "../../components/CustomButton";
import { onUserSignOut } from "../../redux";
import { connect } from "react-redux";
import { AuthContext } from "../../constants/context";
import { Button } from "react-native-paper";
import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import {
  toastError,
  toast500
} from "../../constants/toasts";

/**
 * This component renders the screen where the user can delete their account from the system. The user needs to re-authenticate
 * to access this feature, due to it being critical. To delete their account, they have to confirm the decision.
 * 
 * @param {*} props - used for redux and navigation.
 * 
 * @returns the component. 
 */
const _DeleteAccountScreen = (props) => {
  const { signOutApp } = React.useContext(AuthContext);

  const { userReducer, onUserSignOut } = props;

  const { user } = userReducer;

  const [showIndicator, setShowIndicator] = useState(false);

  const deleteUser = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("email", user.email);

    setShowIndicator(true);

    APIKit.delete("/user", { data: bodyFormData })
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          onUserSignOut({ signOutApp });
          props.navigation.replace("Auth", {
            screen: "SignIn",
          });
        } else {
          toastError("top", 2000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  const createTwoButtonAlert = () =>
    Alert.alert(
      "Deleting user",
      "Deleting user will delete all information about you. Are you sure you will continue?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        { text: "OK", onPress: () => deleteUser() },
      ],
      { cancelable: false }
    );

  return (
    <ScrollView>
      <View style={{ flex: 1, alignItems: "stretch" }}>
        <CustomActivityIndicator
          style={styles.activityIndicator}
          visible={showIndicator}
        />
        <Text style={styles.text}>Are you sure?</Text>
        <Button
          style={styles.button}
          color="red"
          title="Delete"
          mode="contained"
          onPress={createTwoButtonAlert}
        >
          Delete
        </Button>
        <CustomButton
          style={styles.button}
          mode="outlined"
          title="Cancel"
          onPress={() => props.navigation.navigate("Profile")}
        />
      </View>
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const DeleteAccountScreen = connect(mapStateToProps, { onUserSignOut })(
  _DeleteAccountScreen
);

export { DeleteAccountScreen };

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  button: {
    marginTop: 30,
    alignSelf: "center",
    width: "40%",
  },
  text: {
    alignSelf: "center",
    marginTop: 20,
  },
});
