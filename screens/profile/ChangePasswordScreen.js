import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  KeyboardAvoidingView,
} from "react-native";
import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import {
  PASSWORD_REGEX,
  INVALID_PASSWORD_MESSAGE,
  NON_MATCHING_PASSWORDS_MESSAGE,
} from "../../constants/inputRequirements";
import CustomButton from "../../components/CustomButton";
import { TextInput, HelperText } from "react-native-paper";
import {
  toastSuccess, 
  toastError,
  toast500
} from "../../constants/toasts"; 

/**
 * This component renders the screen where a user can change their password. The user needs to re-authenticate to access this
 * feature, due to it being critical. 
 * 
 * @param {*} navigation - extracted from props and used for navigation. 
 *  
 * @returns the component. 
 */
const ChangePasswordScreen = ({ navigation }) => {
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showIndicator, setShowIndicator] = useState(false);
  const [passwordHint, setPasswordhint] = useState(false);
  const [passwordMatchHint, setPasswordMatchHint] = useState(false);

  // To make the back-button on android behave correctly and not go to the VerifyPasswordScreen
  useEffect(() => {
    navigation.addListener('beforeRemove', (e) => {
      e.preventDefault();
      navigation.navigate("Profile");
    });
  }, []);

  const checkPassword = (password) => {
    setPassword(password);
    setPasswordhint(!password.match(PASSWORD_REGEX) && password !== "");
  };

  const checkPasswordMatch = (confirmPassword) => {
    setConfirmPassword(confirmPassword);
    setPasswordMatchHint(
      confirmPassword !== password && confirmPassword !== "" && password !== ""
    );
  };

  const changePassword = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("newPassword", password);
    bodyFormData.append("confirmNewPassword", confirmPassword);

    setShowIndicator(true);

    APIKit.put("/user", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          navigation.navigate("Profile");
          toastSuccess("top", 2000, response.data.message); 
        } else {
          toastError("top", 1000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <KeyboardAvoidingView style={{ flex: 1, alignItems: "stretch" }}>
        <CustomActivityIndicator
          style={styles.activityIndicator}
          visible={showIndicator}
        />
        <View style={styles.textfield}>
          <TextInput
            mode="flat"
            label="New Password"
            type="password"
            secureTextEntry={true}
            placeholder="New Password"
            autoFocus={true}
            onChangeText={(password) => checkPassword(password)}
            value={password}
          />
          {passwordHint && (
            <HelperText type="error" visible={passwordHint}>
              {INVALID_PASSWORD_MESSAGE}
            </HelperText>
          )}
        </View>
        <View style={styles.textfield}>
          <TextInput
            mode="flat"
            label="Confirm Password"
            type="password"
            secureTextEntry={true}
            placeholder="Confirm Password"
            onChangeText={(confirmPassword) =>
              checkPasswordMatch(confirmPassword)
            }
            value={confirmPassword}
          />
          {passwordMatchHint && (
            <HelperText type="error" visible={passwordMatchHint}>
              {NON_MATCHING_PASSWORDS_MESSAGE}
            </HelperText>
          )}
        </View>
        <CustomButton
          style={styles.button}
          title="Confirm"
          mode="contained"
          onPress={() => changePassword()}
          disabled={
            !(
              !passwordHint &&
              !passwordMatchHint &&
              confirmPassword !== "" &&
              password !== ""
            )
          }
        />
        <CustomButton
          style={styles.button}
          title="Cancel"
          mode="outlined"
          onPress={() => navigation.navigate("Profile")}
        />
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default ChangePasswordScreen;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  button: {
    width: "40%",
    alignSelf: "center",
    marginTop: 20,
  },
  textfield: {
    marginTop: 5,
    width: "80%",
    alignSelf: "center",
  },
});
