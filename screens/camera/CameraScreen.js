import React, { useState, useEffect } from "react";
import { StyleSheet, ScrollView, View, Image, Dimensions } from "react-native";
import { Camera } from "expo-camera";
import mime from "mime";
import * as ImageManipulator from 'expo-image-manipulator';
import CustomCamera from "../../components/CustomCamera";
import CustomImagePicker from "../../components/CustomImagePicker";
import ImagePreview from "../../components/ImagePreview";
import CustomButton from "../../components/CustomButton";
import { IconButton } from "react-native-paper";
import * as FileSystem from "expo-file-system";
import APIKit from "../../APIkit";
import { updateLocations, addLastLocation } from "../../redux";
import { connect } from "react-redux";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { toast500, toastError } from "../../constants/toasts";

const deviceWidth = Dimensions.get("window").width;

/**
 * This component renders the camera screen. This screen consists of an image picker, which makes the user able to upload images from their phones
 * library. It also consists of a built-in camera, where the user can take a photo while in the application. The user might need to grant premission for
 * the app to be able to use the camera. It also will rescale the image before upload, to make the upload go faster.
 *
 * @param {*} props - used for redux and navigation.
 *
 * @returns the component
 */
const _CameraScreen = (props) => {
  const [isCameraMode, setShowCamera] = useState(false);
  const [isPreviewMode, setPreviewMode] = useState(false);
  const [image, setImage] = useState(null);
  const [hasPermission, setHasPermission] = useState(null);
  const [showIndicator, setShowIndicator] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  const deletePictureFromCache = () => {
    if (image) {
      FileSystem.deleteAsync(image);
    }
  };

  const uploadImage = async () => {
    setShowIndicator(true);
    const newImageUri = "file:///" + image.split("file:/").join("");

    Image.getSize(
      newImageUri,
      (width, height) => {
        let maxSize = 1280;
        let largerDim = Math.max(width, height);

        if (largerDim > maxSize) {
          let scalingFactor = maxSize / largerDim;

          ImageManipulator.manipulateAsync(
            newImageUri,
            [{ resize: {width: width*scalingFactor, height: height*scalingFactor} }],
            {format: ImageManipulator.SaveFormat.PNG }
          )
            .then((response) => {
              let bodyFormData = new FormData();
              bodyFormData.append("image", {
                uri: response.uri,
                type: mime.getType(response.uri),
                name: newImageUri.split("/").pop(),
              });

              APIKit.post("/findSalamanderInfo", bodyFormData)
                .then(function (response) {
                  setShowIndicator(false);
                  if (response.data.status === 200) {
                    setImage(null);
                    props.navigation.navigate("Verify", {
                      screen: "RegisterSalamander",
                      params: {
                        salamanderSex: response.data.sex,
                        salamanderSpecies: response.data.species,
                        image: response.data.image,
                      },
                    });
                  } else {
                    toastError("top", 3000, response.data.message);
                  }
                })
                .catch(function (response) {
                  setShowIndicator(false);
                  toast500(response.status);
                });
            })
            .catch((err) => {
              console.log(err, "Error."); 
            });
        } else {
          let bodyFormData = new FormData();
          bodyFormData.append("image", {
            uri: newImageUri,
            type: mime.getType(newImageUri),
            name: newImageUri.split("/").pop(),
          });

          APIKit.post("/findSalamanderInfo", bodyFormData)
            .then(function (response) {
              setShowIndicator(false);
              if (response.data.status === 200) {
                setImage(null);
                props.navigation.navigate("Verify", {
                  screen: "RegisterSalamander",
                  params: {
                    salamanderSex: response.data.sex,
                    salamanderSpecies: response.data.species,
                    image: response.data.image,
                  },
                });
              } else {
                toastError("top", 3000, response.data.message);
              }
            })
            .catch(function (response) {
              setShowIndicator(false);
              toast500(response.status);
            });
        }
      }
    );
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.takeImage}>
          <CustomActivityIndicator
            style={styles.activityIndicator}
            text="Processing image. Please wait."
            visible={showIndicator}
          />
          <CustomImagePicker setImageUri={(uri) => setImage(uri)} />
          <IconButton
            icon="camera"
            color="#435768"
            size={90}
            onPress={() => {
              setShowCamera(true);
            }}
          />
          {isCameraMode && (
            <CustomCamera
              showModal={isCameraMode}
              setModalVisible={() => {
                setShowCamera(false);
              }}
              deleteImage={() => {
                deletePictureFromCache();
              }}
              setError={(errorString) => setError(errorString)}
              setImageUri={(uri) => setImage(uri)}
              permission={hasPermission}
            />
          )}
        </View>
        {image && (
          <TouchableWithoutFeedback
            style={styles.previewContainer}
            onPress={() => setPreviewMode(true)}
          >
            <Image
              source={{ uri: image }}
              style={{
                flex: 1,
                width: undefined,
                height: undefined,
                resizeMode: "contain",
              }}
            />
          </TouchableWithoutFeedback>
        )}
        {isPreviewMode && (
          <ImagePreview
            showModal={isPreviewMode}
            setModalVisible={() => {
              setPreviewMode(false);
            }}
            img={image}
          />
        )}
        {image && (
          <CustomButton
            title="Start Processing"
            mode="contained"
            style={styles.button}
            onPress={() => {
              uploadImage();
            }}
          />
        )}
      </View>
    </ScrollView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const CameraScreen = connect(mapStateToProps, {
  updateLocations,
  addLastLocation,
})(_CameraScreen);

export { CameraScreen };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 25,
  },
  button: {
    width: 200,
    marginTop: 50,
    marginBottom: 20,
    alignSelf: "center",
  },
  dropdown: {
    margin: 20,
  },
  takeImage: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginStart: 30,
    marginEnd: 30,
  },
  previewContainer: {
    flex: 1,
    alignSelf: "center",
    backgroundColor: "transparent",
    height: deviceWidth * 0.8,
    width: deviceWidth * 0.8,
  },
  activityIndicator: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: `rgba(0,0,0,${0.5})`,
  },
  text: {
    marginStart: 20,
  },
});
