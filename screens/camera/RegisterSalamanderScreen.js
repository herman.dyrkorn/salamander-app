import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
  Image,
} from "react-native";
import { TextInput, HelperText, RadioButton, Colors } from "react-native-paper";

import CustomDropDown from "../../components/CustomDropDown";
import CustomButton from "../../components/CustomButton";
import MatchResponse from "../../components/MatchResponse";
import ImagePreview from "../../components/ImagePreview";

import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import { updateLocations, addLastLocation } from "../../redux";
import { connect } from "react-redux";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import {
  NUMBER_MAX_LENGTH,
  MEASUREMENT_REGEX,
  INVALID_MEASUREMENT_MESSAGE,
} from "../../constants/inputRequirements";
import { toast500, toastError } from "../../constants/toasts";
import CustomRadioButtonGroup from "../../components/CustomRadioButtonGroup";

const species = [
  { label: "Northern Crested Newt", value: "northern_crested_newt" },
  { label: "Smooth Newt", value: "smooth_newt" },
];

const sexValues = {firstValue:"female", secondValue:"male", thirdValue:"juvenile", 
firstText:"Female", secondText:"Male", thirdText:"Juvenile"};

/**
 * This component renders the screen where the user can register a new salamander entry. When the user has provided all mandatory data, 
 * the salamander will be matched agaist the other registered salamander with the same data (location, species, sex). If there is a match, 
 * a green popup screen will show, providing information about which salamander it was matched with. If there was no match, a white poput will show, 
 * and a new salamander will be registered in the database with a new ID. 
 * 
 * @param {*} props - used for redux, navigation and route to get data from the previous screen.
 * 
 * @returns the component.
 */
const _RegisterSalamanderScreen = (props) => {
  const { salamanderSex, salamanderSpecies, image } = props.route.params;
  const { userReducer, updateLocations, addLastLocation } = props;
  const { locationList } = userReducer;

  const { lastLocation } = userReducer;

  const [length, setLength] = useState(null);
  const [weight, setWeight] = useState(null);
  const [locationValue, setLocationValue] = useState(lastLocation);
  const [speciesValue, setSpeciesValue] = useState(null);
  const [sexValue, setSexValue] = useState(null);
  const [visible, setVisible] = useState(false);
  const [isPreviewMode, setPreviewMode] = useState(false);
  const [showIndicator, setShowIndicator] = useState(false);
  const [response, setResponse] = useState({
    responseMatch: "",
    id: "",
    responseText: "",
    responseColor: "",
  });
  const [weightHint, setWeightHint] = useState(false);
  const [lengthHint, setLengthHint] = useState(false);

  const checkWeight = (weight) => {
    setWeight(weight);
    setWeightHint(!weight.match(MEASUREMENT_REGEX) && weight !== "");
  };

  const checkLength = (length) => {
    setLength(length);
    setLengthHint(!length.match(MEASUREMENT_REGEX) && length !== "");
  };

  const hideDialogHandler = () => {
    props.navigation.navigate("Camera");
  };

  const getLocations = () => {
    let locationNames = [];
    locationList.map((location) => {
      locationNames.push({
        label: location.name.charAt(0).toUpperCase() + location.name.slice(1),
        value: location.name,
      });
    });

    return locationNames;
  };

  const getLocationValue = (value) => {
    setLocationValue(value);
  };

  const getSpeciesValue = (value) => {
    setSpeciesValue(value);
  };

  const getSexValue = (value) => {
    setSexValue(value);
  };

  const locations = getLocations();

  const matchSalamander = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("sex", sexValue);
    bodyFormData.append("location", locationValue);
    bodyFormData.append("species", speciesValue);

    if (length != null) {
      bodyFormData.append("length", length);
    }

    if (weight != null) {
      bodyFormData.append("weight", weight);
    }

    setShowIndicator(true);

    APIKit.post("/matchSalamander", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          addLastLocation({ locationValue: locationValue });
          if (response.data.matching === "Match!") {
            setResponse({
              responseMatch: response.data.matching,
              id: response.data.id,
              responseText: response.data.message,
              responseColor: Colors.green300,
            });
          } else {
            setResponse({
              responseMatch: response.data.matching,
              id: response.data.id,
              responseText: response.data.message,
              responseColor: Colors.white,
            });
          }
          setVisible(true);
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : null}
      style={{ flex: 1 }}
    >
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <CustomActivityIndicator
            style={styles.activityIndicator}
            visible={showIndicator}
          />
          <MatchResponse
            visible={visible}
            onCancel={hideDialogHandler}
            responseMatch={response.responseMatch}
            id={response.id}
            responseText={response.responseText}
            responseColor={response.responseColor}
          />
          <Text style={styles.text}>Location</Text>
          <View
            style={{
              ...(Platform.OS !== "android" && {
                zIndex: 20,
              }),
              margin: 10,
            }}
          >
            <CustomDropDown
              defaultValue={locationValue}
              myItems={locations}
              sendDataToParent={getLocationValue}
            />
          </View>
          <Text style={styles.text}>Species</Text>
          <View
            style={{
              ...(Platform.OS !== "android" && {
                zIndex: 15,
              }),
              margin: 10,
            }}
          >
            <CustomDropDown
              myItems={species}
              sendDataToParent={getSpeciesValue}
            />
          </View>
          <Text style={styles.text}>Sex</Text>
          <View style={{marginStart: 20}}>
            <CustomRadioButtonGroup 
              default={sexValue}
              values={sexValues}
              sendDataToParent={getSexValue}
            />
          </View>
          <View style={styles.textfieldContainer}>
            <View style={styles.textfield}>
              <TextInput
                onChangeText={(length) => checkLength(length)}
                value={length}
                keyboardType="decimal-pad"
                mode="flat"
                placeholder="Length"
                label="Length (Optional)"
                maxLength={NUMBER_MAX_LENGTH}
              />
              {lengthHint && (
                <HelperText type="error" visible={lengthHint}>
                  {INVALID_MEASUREMENT_MESSAGE}
                </HelperText>
              )}
            </View>
            <Text style={styles.textfieldText}>Centimeters</Text>
          </View>
          <View style={styles.textfieldContainer}>
            <View style={styles.textfield}>
              <TextInput
                onChangeText={(weight) => checkWeight(weight)}
                keyboardType="decimal-pad"
                value={weight}
                mode="flat"
                placeholder="Weight"
                label="Weight (Optional)"
                maxLength={NUMBER_MAX_LENGTH}
              />
              {weightHint && (
                <HelperText type="error" visible={weightHint}>
                  {INVALID_MEASUREMENT_MESSAGE}
                </HelperText>
              )}
            </View>
            <Text style={styles.textfieldText}>Gram</Text>
          </View>
          <TouchableWithoutFeedback
            style={styles.previewContainer}
            onPress={() => setPreviewMode(true)}
          >
            <Image
              style={{
                height: 120,
                width: 320,
                alignSelf: "center",
                marginTop: 50,
              }}
              source={{ uri: `data:image/png;base64,${image}` }}
            />
          </TouchableWithoutFeedback>
          {isPreviewMode && (
            <ImagePreview
              showModal={isPreviewMode}
              setModalVisible={() => {
                setPreviewMode(false);
              }}
              img={`data:image/png;base64,${image}`}
            />
          )}
          <View style={styles.buttonContainer}>
            <CustomButton
              style={styles.button}
              title="Cancel"
              mode="outlined"
              onPress={() => props.navigation.navigate("Camera")}
            />
            <CustomButton
              style={styles.button}
              title="Confirm"
              mode="contained"
              onPress={() => matchSalamander()}
              disabled={
                !(
                  !weightHint &&
                  !lengthHint &&
                  locationValue != null &&
                  speciesValue != null &&
                  sexValue != null
                )
              }
            />
          </View>
        </SafeAreaView>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const RegisterSalamanderScreen = connect(mapStateToProps, { updateLocations, addLastLocation })(
  _RegisterSalamanderScreen
);

export { RegisterSalamanderScreen };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    marginTop: 30,
    marginBottom: 60
  },
  button: {
    width: "40%",
    alignSelf: "center",
    marginBottom: 20,
  },
  textfield: {
    marginTop: 15,
    width: "50%",
    alignSelf: "flex-start",
    marginStart: 20,
  },
  textfieldContainer: {
    flexDirection: "row",
    marginTop: 15,
    marginStart: 20,
  },
  textfieldText: {
    flex: 1,
    alignSelf: "flex-end",
    fontSize: 18,
    marginStart: 20,
    paddingBottom: 10,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 40,
    marginEnd: 30,
    marginStart: 30,
  },
  text: {
    marginStart: 20,
  },
  radioButton: {
    flexDirection: "row",
    alignItems: "center",
  },
});
