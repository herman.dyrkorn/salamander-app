import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import { TextInput, HelperText, Button } from "react-native-paper";
import {
  EMAIL_REGEX,
  PASSWORD_REGEX,
  INVALID_EMAIL_MESSAGE,
  INVALID_PASSWORD_MESSAGE,
  NON_MATCHING_PASSWORDS_MESSAGE,
  NAME_MAX_LENGTH,
} from "../constants/inputRequirements";
import APIKit from "../APIkit";
import CustomActivityIndicator from "../components/CustomActivityIndicator";
import { toast500, toastError, toastSuccess } from "../constants/toasts";

/**
 * This component renders the screen where the user can create a new user. They will be navigated to the sign in screen when the user is
 * created, and has to wait for an administrator to grant them access to use the application. 
 * 
 * @param {*} navigation - extracted from props, used for navigation
 * 
 * @returns the component. 
 */
const SignUpScreen = ({ navigation }) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [emailHint, setEmailHint] = useState(false);
  const [passwordHint, setPasswordhint] = useState(false);
  const [passwordMatchHint, setPasswordMatchHint] = useState(false);
  const [showIndicator, setShowIndicator] = useState(false);

  const checkPassword = (password) => {
    setPassword(password);
    setPasswordhint(!password.match(PASSWORD_REGEX) && password !== "");
  };

  const checkPasswordMatch = (confirmPassword) => {
    setConfirmPassword(confirmPassword);
    setPasswordMatchHint(
      confirmPassword !== password && confirmPassword !== "" && password !== ""
    );
  };

  const checkEmail = (email) => {
    setEmail(email);
    setEmailHint(!email.match(EMAIL_REGEX) && email !== "");
  };

  const signUp = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("name", name);
    bodyFormData.append("email", email);
    bodyFormData.append("password", password);
    bodyFormData.append("confirmPassword", confirmPassword);

    setShowIndicator(true);

    APIKit.post("/user", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          navigation.navigate("SignIn");
          toastSuccess("top", 3000, response.data.message); 
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : null}
      style={{ flex: 1 }}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <SafeAreaView style={styles.container}>
          <CustomActivityIndicator
            style={styles.activityIndicator}
            visible={showIndicator}
          />
          <View style={styles.image}>
            <Image
              style={styles.logo}
              source={require("../assets/images/ninaLogo.png")}
            />
          </View>
          <TextInput
            style={styles.textfield}
            autoFocus={true}
            mode="outlined"
            placeholder="Name"
            label="Name"
            autoCapitalize="words"
            onChangeText={(name) => setName(name)}
            value={name}
            maxLength={NAME_MAX_LENGTH}
          />
          <View style={styles.textfield}>
            <TextInput
              style={styles.textfield}
              mode="outlined"
              placeholder="Email"
              label="Email"
              autoCapitalize="none"
              keyboardType="email-address"
              onChangeText={(email) => {
                checkEmail(email);
              }}
              value={email}
              maxLength={NAME_MAX_LENGTH}
            />
            {emailHint && (
              <HelperText type="error" visible={emailHint}>
                {INVALID_EMAIL_MESSAGE}
              </HelperText>
            )}
          </View>
          <View style={styles.textfield}>
            <TextInput
              mode="outlined"
              secureTextEntry={true}
              placeholder="Password"
              label="password"
              onChangeText={(password) => {
                checkPassword(password);
              }}
              value={password}
            />
            {passwordHint && (
              <HelperText type="error" visible={passwordHint}>
                {INVALID_PASSWORD_MESSAGE}
              </HelperText>
            )}
          </View>
          <View style={styles.textfield}>
            <TextInput
              style={styles.textfield}
              mode="outlined"
              secureTextEntry={true}
              label="Confirm Password"
              onChangeText={(confirmPassword) => {
                checkPasswordMatch(confirmPassword);
              }}
              value={confirmPassword}
            />
            {passwordMatchHint && (
              <HelperText type="error" visible={passwordMatchHint}>
                {NON_MATCHING_PASSWORDS_MESSAGE}
              </HelperText>
            )}
          </View>
          <Button
            style={styles.button}
            mode="contained"
            onPress={() => signUp()}
            disabled={
              !(
                !passwordHint &&
                !passwordMatchHint &&
                !emailHint &&
                confirmPassword !== "" &&
                password !== "" &&
                email !== ""
              )
            }
          >Sign Up</Button>
          <TouchableOpacity onPress={() => navigation.navigate("SignIn")}>
            <Text style={styles.text}>Already have a user?</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

export default SignUpScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    justifyContent: "center",
    backgroundColor: "lightblue",
  },
  textfield: {
    margin: 2,
    width: 300,
    alignSelf: "center",
  },

  button: {
    marginTop: 15,
    margin: 50,
    width: "50%",
    alignSelf: "center",
  },
  text: {
    alignSelf: "center",
    marginTop: 15,
    fontSize: 15,
  },
  logo: {
    width: 190,
    height: 100,
  },
  image: {
    alignItems: "center",
    paddingBottom: 15,
  },
  scrollView: {
    backgroundColor: "lightblue",
  },
});
