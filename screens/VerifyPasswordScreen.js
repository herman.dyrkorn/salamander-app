import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  SafeAreaView,
} from "react-native";
import { TextInput } from "react-native-paper";
import CustomButton from "../components/CustomButton";
import APIKit from "../APIkit";
import CustomActivityIndicator from "../components/CustomActivityIndicator";
import { toast500, toastError } from "../constants/toasts";

/**
 * This component renders the screen where the user needs to re-authenticate to get access to a critical feature. 
 * 
 * @param {*} route - extracted from props, used to get data from the previous screen. In this case, to know which screen should be 
 *                    navigated to next. 
 * @param {*} navigation - extracted from props, used for navigation. 
 *  
 * @returns the component. 
 */
const VerifyPasswordScreen = ({ route, navigation }) => {
  const { nextNavigation } = route.params;

  const [password, setPassword] = useState("");
  const [showIndicator, setShowIndicator] = useState(false);

  const verifyPassword = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("password", password);

    setShowIndicator(true);
    APIKit.post("/verifyPassword", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          navigation.navigate(nextNavigation);
        } else {
          toastError("top", 1000, "Wrong password"); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : null}
      style={{ flex: 1 }}
    >
      <ScrollView keyboardShouldPersistTaps="handled">
        <SafeAreaView style={{ flex: 1, alignItems: "stretch" }}>
          <CustomActivityIndicator
            style={styles.activityIndicator}
            visible={showIndicator}
          />
          <Text style={styles.title}>Please verify your password</Text>
          <TextInput
            style={styles.textfield}
            autoFocus
            mode="flat"
            label="Password"
            secureTextEntry={true}
            placeholder="Password"
            type="password"
            onChangeText={(password) => setPassword(password)}
            value={password}
            onSubmitEditing={verifyPassword}
          />
          <CustomButton
            style={styles.button}
            title="Confirm"
            mode="contained"
            onPress={() => verifyPassword()}
          />
          <CustomButton
            style={styles.button}
            title="Cancel"
            mode="outlined"
            onPress={() => navigation.goBack()}
          />
        </SafeAreaView>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default VerifyPasswordScreen;

const styles = StyleSheet.create({
  title: {
    fontWeight: "bold",
    fontSize: 14,
    marginTop: 30,
    marginStart: 3,
    alignSelf: "center",
  },
  text: {
    fontSize: 16,
  },
  button: {
    width: "40%",
    alignSelf: "center",
    marginTop: 20,
  },
  textfield: {
    width: "80%",
    alignSelf: "center",
  },
  appbar: {
    justifyContent: "space-between",
  },
});
