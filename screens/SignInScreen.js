import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  SafeAreaView,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import { TextInput } from "react-native-paper";
import { AuthContext } from "../constants/context";
import { onUserLogin } from "../redux";
import { Button } from "react-native-paper";

import CustomActivityIndicator from "../components/CustomActivityIndicator";
import { NAME_MAX_LENGTH } from "../constants/inputRequirements";

/**
 * This component renders the screen where the user can sign in. The actual sign in process is done in a user action through redux. 
 * 
 * @param {*} props - used for redux and navigation. 
 * 
 * @returns the component. 
 */
const _SignInScreen = (props) => {
  const { onUserLogin } = props;

  const { signInApp } = React.useContext(AuthContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showIndicator, setShowIndicator] = useState(false);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : null}
      style={{ flex: 1 }}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <SafeAreaView style={styles.container}>
          <View style={styles.image}>
            <Image
              style={styles.logo}
              source={require("../assets/images/ninaLogo.png")}
            />
            <CustomActivityIndicator visible={showIndicator} />
          </View>
          <View style={styles.textfield}>
            <TextInput
              style={styles.textfield}
              mode="outlined"
              placeholder="Email"
              autofocus
              type="email"
              autoCapitalize="none"
              label="Email"
              keyboardType="email-address"
              onChangeText={(email) => setEmail(email)}
              value={email}
              maxLength={NAME_MAX_LENGTH}
            />
          </View>
          <TextInput
            style={styles.textfield}
            mode="outlined"
            secureTextEntry={true}
            placeholder="Password"
            label="password"
            type="password"
            onChangeText={(password) => setPassword(password)}
            value={password}
          />
          <Button
            style={styles.button}
            mode="contained"
            onPress={() =>
              onUserLogin({
                email: email,
                password: password,
                signInApp,
                setShowIndicator,
              })
            }
          >Sign In</Button>
          <TouchableOpacity onPress={() => props.navigation.navigate("SignUp")}>
            <Text style={styles.text}>Don't have a user?</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const SignInScreen = connect(mapStateToProps, { onUserLogin })(_SignInScreen);

export { SignInScreen };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    justifyContent: "center",
    backgroundColor: "lightblue",
  },
  textfield: {
    margin: 3,
    width: 300,
    alignSelf: "center",
  },
  button: {
    marginTop: 15,
    margin: 50,
    width: "50%",
    alignSelf: "center",
  },
  text: {
    alignSelf: "center",
    marginTop: 15,
    fontSize: 15,
  },
  logo: {
    width: 190,
    height: 100,
  },
  image: {
    alignItems: "center",
    paddingBottom: 15,
  },
  scrollView: {
    backgroundColor: "lightblue",
  },
});
