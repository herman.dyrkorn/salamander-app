import React, { useEffect, useState } from "react";
import { StyleSheet, SafeAreaView, View } from "react-native";
import MapView from "react-native-maps";
import { PROVIDER_GOOGLE } from "react-native-maps";
import { Marker, Circle } from "react-native-maps";
import { updateLocations } from "../../redux";
import { connect } from "react-redux";
import { FAB } from "react-native-paper";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";

/**
 * This component renders the home screen. This screen consists primarily of a map, and the user can either register a new location or edit an
 * existing one from this screen. The user can also see all registered locations, and if they have given permission to the app to use their location, 
 * their current location will be shown on the map. 
 * 
 * @param {*} props - used for redux and navigation. 
 * 
 * @returns the component. 
 */
const _HomeScreen = (props) => {
  const { userReducer, updateLocations } = props;

  const { locationList } = userReducer;

  const [lat, setLat] = useState(61.044135);
  const [lng, setLng] = useState(9.130127);
  const [showIndicator, setShowIndicator] = useState(false);
  const [locationData, setLocationData] = useState({});
  const [showButtonEdit, setShowButtonEdit] = useState(false);
  const [showButtonAdd, setShowButtonAdd] = useState(false);
  const [displayRegisterMarker, setRegisterMarker] = useState(false);

  const setLocation = (lat, lng) => {
    setLat(parseFloat(lat.toFixed(6)));
    setLng(parseFloat(lng.toFixed(6)));
  };

  const getCurrentLocation = async () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        setLocation(position.coords.latitude, position.coords.longitude);
      }
    );
  };

  const editLocation = (loc) => {
    setLocationData(loc);
    setShowButtonAdd(false); 
    setShowButtonEdit(true);
  };

  const refreshLocations = () => {
    setShowButtonEdit(false);
    updateLocations({ setShowIndicator });
  };

  useEffect(() => {
    getCurrentLocation();
    updateLocations({ setShowIndicator });
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <CustomActivityIndicator
        style={styles.activityIndicator}
        visible={showIndicator}
      />
      <MapView
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        showsUserLocation={true}
        showsMyLocationButton={true}
        initialRegion={{
          longitude: lng,
          latitude: lat,
          longitudeDelta: 8,
          latitudeDelta: 8,
        }}
        onLongPress={(e) => {
          setLocation(
            e.nativeEvent.coordinate.latitude,
            e.nativeEvent.coordinate.longitude
          );
          setRegisterMarker(true);
          setShowButtonEdit(false); 
          setShowButtonAdd(true); 
        }}
      >
        {locationList.map((location) => (
          <Circle
            radius={location.radius}
            key={location.id}
            center={{
              latitude: location.latitude,
              longitude: location.longitude,
            }}
            fillColor={"rgba(255, 5, 5, 0.5)"}
          ></Circle>
        ))}
        {locationList.map((location) => (
          <Marker
            key={location.id}
            onPress={() => {
              editLocation(location);
            }}
            coordinate={{
              latitude: location.latitude,
              longitude: location.longitude,
            }}
            title={location.name}
          />
        ))}
        {displayRegisterMarker && (
          <Marker
            coordinate={{ latitude: lat, longitude: lng }}
          />
        )} 
      </MapView>
      <View style={styles.fabRefresh}>
        <FAB
          style={styles.fab}
          label="Refresh"
          icon="refresh"
          onPress={() => refreshLocations()}
        />
      </View>
      {showButtonEdit && (
        <View style={styles.fabButtonLocation}>
          <FAB
            style={styles.fab}
            label="Edit Location"
            onPress={() => {
              setShowButtonEdit(false);
              props.navigation.navigate("Verify", {
                screen: "EditLocation",
                params: {
                  locationId: locationData.id,
                  locationName: locationData.name,
                  locationRadius: locationData.radius,
                },
              });
            }}
          />
        </View>
      )}
      {showButtonAdd && (
        <View style={styles.fabButtonLocation}>
          <FAB
            style={styles.fab}
            label="Add Location"
            onPress={() => {
              setShowButtonAdd(false);
              props.navigation.navigate("Verify", {
                screen: "RegisterLocation",
                params: { longitude: lng, latitude: lat },
              });
            }}
          />
        </View>
      )}
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const HomeScreen = connect(mapStateToProps, { updateLocations })(_HomeScreen);

export { HomeScreen };

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  map: {
    width: "100%",
    height: "100%",
  },
  button: {
    width: 150,
    alignSelf: "center",
  },
  fab: {
    position: "absolute",
    alignSelf: "center",
    backgroundColor: "rgba(250,250,250,0.9)",
  },
  fabRefresh: {
    position: "absolute",
    top: "2%",
    alignSelf: "center",
  },
  fabButtonLocation: {
    position: "absolute",
    bottom: "12%",
    alignSelf: "center",
  },
});
