import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
} from "react-native";

import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import CustomButton from "../../components/CustomButton";
import { TextInput, HelperText } from "react-native-paper";
import { updateLocations } from "../../redux";
import { connect } from "react-redux";
import {
  LOCATION_NUMBER_MAX_LENGTH,
  INTEGER_REGEX,
  INVALID_INTEGER_MESSAGE,
  NAME_MAX_LENGTH,
  LEGAL_LOCATION_REGEX,
  ILLEGAL_LOCATION_MESSAGE,
} from "../../constants/inputRequirements";
import { toast500, toastError, toastSuccess } from "../../constants/toasts";

/**
 * This component renders the screen where a user can register a new location. The user will have to provide a name and a radius for the 
 * new location. When the location is approved, the user will be taken back to the home screen. 
 * 
 * @param {*} props - used for redux, navigation and route to get data from the previous screen.
 * 
 * @returns the component. 
 */
const _RegisterLocationScreen = (props) => {
  const { longitude, latitude } = props.route.params;

  const { updateLocations } = props;

  const [name, setName] = useState("");
  const [radius, setRadius] = useState("");
  const [showIndicator, setShowIndicator] = useState(false);
  const [radiusHint, setRadiusHint] = useState(false);
  const [nameHint, setNameHint] = useState(false);

  const checkRadius = (radius) => {
    setRadius(radius);
    setRadiusHint(!radius.match(INTEGER_REGEX) && radius !== "");
  };

  const checkName = (name) => {
    setName(name);
    setNameHint(!name.match(LEGAL_LOCATION_REGEX) && name !== "");
  };

  const registerLocation = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("name", name);
    bodyFormData.append("radius", radius);
    bodyFormData.append("longitude", longitude);
    bodyFormData.append("latitude", latitude);

    setShowIndicator(true);

    APIKit.post("/location", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          updateLocations({ setShowIndicator });
          props.navigation.navigate("Home");
          toastSuccess("top", 1000, response.data.message); 
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : null}
      style={{ flex: 1 }}
    >
      <ScrollView style={styles.scrollView} keyboardShouldPersistTaps="handled">
        <SafeAreaView style={styles.container}>
          <CustomActivityIndicator
            style={styles.activityIndicator}
            visible={showIndicator}
          />
          <Text style={{ fontSize: 18, marginStart: 20, marginBottom: 10 }}>
            Location Data
          </Text>
          <TextInput
            onChangeText={(name) => setName(name)}
            style={styles.textfieldLocation}
            autoFocus={true}
            mode="flat"
            placeholder="Location name"
            label="Location name"
            onChangeText={(name) => checkName(name)}
            maxLength={NAME_MAX_LENGTH}
          />
          {nameHint && (
            <HelperText type="error" visible={nameHint}>
              {ILLEGAL_LOCATION_MESSAGE}
            </HelperText>
          )}
          <View style={styles.radiusContainer}>
            <View style={styles.textfieldRadius}>
              <TextInput
                mode="flat"
                placeholder="Radius"
                label="Radius"
                keyboardType="number-pad"
                onChangeText={(radius) => checkRadius(radius)}
                maxLength={LOCATION_NUMBER_MAX_LENGTH}
              />
              {radiusHint && (
                <HelperText type="error" visible={radiusHint}>
                  {INVALID_INTEGER_MESSAGE}
                </HelperText>
              )}
            </View>
            <Text style={styles.radiusText}>Meter</Text>
          </View>
          <View style={styles.buttonContainer}>
            <CustomButton
              style={styles.button}
              title="Cancel"
              mode="outlined"
              onPress={() => props.navigation.navigate("Home")}
            />
            <CustomButton
              style={styles.button}
              mode="contained"
              title="Confirm"
              onPress={() => registerLocation()}
              disabled={
                !(!radiusHint && radius !== "" && name !== "" && !nameHint)
              }
            />
          </View>
        </SafeAreaView>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const RegisterLocationScreen = connect(mapStateToProps, { updateLocations })(
  _RegisterLocationScreen
);

export { RegisterLocationScreen };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
  },
  button: {
    width: "40%",
    alignSelf: "center",
    marginBottom: 20,
  },
  textfieldLocation: {
    marginTop: 5,
    margin: 20,
  },
  textfieldRadius: {
    flex: 1,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 50,
    marginEnd: 20,
    marginStart: 20,
  },
  radiusContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginStart: 20,
  },
  radiusText: {
    flex: 1,
    alignSelf: "flex-end",
    fontSize: 18,
    marginStart: 20,
    paddingBottom: 10,
  },
});
