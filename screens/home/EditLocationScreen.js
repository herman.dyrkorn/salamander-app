import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
  Alert,
} from "react-native";
import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import CustomButton from "../../components/CustomButton";
import { TextInput, HelperText, Divider, Button } from "react-native-paper";
import { updateLocations } from "../../redux";
import { connect } from "react-redux";
import {
  NUMBER_MAX_LENGTH,
  INTEGER_REGEX,
  INVALID_INTEGER_MESSAGE,
  NAME_MAX_LENGTH,
  LEGAL_LOCATION_REGEX,
  ILLEGAL_LOCATION_MESSAGE,
} from "../../constants/inputRequirements";
import { toast500, toastError, toastSuccess } from "../../constants/toasts";

/**
 * This component renders the screen where a user can edit an existing location. The user can either edit the name, the radius or, if there
 * are no salamanders at the given location, delete the location from the system. 
 * 
 * @param {*} props - used for redux, navigation and route to get data from the previous screen.
 *  
 * @returns the component
 */
const _EditLocationScreen = (props) => {
  const { locationId, locationName, locationRadius } = props.route.params;

  const [name, setName] = useState(locationName);
  const [radius, setRadius] = useState(locationRadius.toString());
  const [showIndicator, setShowIndicator] = useState(false);
  const [radiusHint, setRadiusHint] = useState(false);
  const [nameHint, setNameHint] = useState(false);

  const { updateLocations } = props;

  const checkRadius = (radius) => {
    setRadius(radius);
    setRadiusHint(!radius.match(INTEGER_REGEX) && radius !== "");
  };

  const checkName = (name) => {
    setName(name);
    setNameHint(!name.match(LEGAL_LOCATION_REGEX) && name !== "");
  };

  const deleteLocation = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("id", locationId);

    setShowIndicator(true);

    APIKit.delete("/location", { data: bodyFormData })
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          updateLocations({ setShowIndicator });
          props.navigation.navigate("Home");
          toastSuccess("top", 2000, response.data.message); 
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  const editLocation = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("id", locationId);
    bodyFormData.append("new_name", name);
    bodyFormData.append("new_radius", radius);

    setShowIndicator(true);

    APIKit.put("/location", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          updateLocations({ setShowIndicator });
          props.navigation.navigate("Home");
          toastSuccess("top", 2000, response.data.message); 
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  const createTwoButtonAlert = () =>
    Alert.alert(
      "Deleting Location",
      "Deleting this location will remove it from the system. If there are salamanders at this location, you will not be able to delete it. Are you sure you want to continue?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        { text: "OK", onPress: () => deleteLocation() },
      ],
      { cancelable: false }
    );

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : null}
      style={{ flex: 1 }}
    >
      <ScrollView style={styles.scrollView} keyboardShouldPersistTaps="handled">
        <SafeAreaView style={styles.container}>
          <CustomActivityIndicator
            style={styles.activityIndicator}
            visible={showIndicator}
          />
          <Text style={{ fontSize: 18, marginStart: 20, marginBottom: 10 }}>
            Location Data
          </Text>
          <TextInput
            onChangeText={(name) => setName(name)}
            style={styles.textfieldLocation}
            value={name}
            mode="flat"
            placeholder="Location name"
            label="Location name"
            onChangeText={(name) => checkName(name)}
            maxLength={NAME_MAX_LENGTH}
          />
          {nameHint && (
            <HelperText type="error" visible={nameHint}>
              {ILLEGAL_LOCATION_MESSAGE}
            </HelperText>
          )}
          <View style={styles.radiusContainer}>
            <TextInput
              style={styles.textfieldRadius}
              mode="flat"
              value={radius}
              placeholder="Radius"
              label="Radius"
              keyboardType="number-pad"
              onChangeText={(radius) => checkRadius(radius)}
              maxLength={NUMBER_MAX_LENGTH}
            />
            {radiusHint && (
              <HelperText type="error" visible={radiusHint}>
                {INVALID_INTEGER_MESSAGE}
              </HelperText>
            )}
            <Text style={styles.radiusText}>Meter</Text>
          </View>
          <View style={styles.buttonContainer}>
            <CustomButton
              style={styles.button}
              title="Cancel"
              mode="outlined"
              onPress={() => props.navigation.navigate("Home")}
            />
            <CustomButton
              style={styles.button}
              mode="contained"
              title="Confirm"
              onPress={() => editLocation()}
              disabled={
                !(!radiusHint && radius !== "" && name !== "" && !nameHint)
              }
            />
          </View>
          <Divider style={{ marginTop: 20, marginBottom: 20 }} />
          <Button
            style={styles.button}
            color="red"
            title="Delete"
            mode="contained"
            onPress={createTwoButtonAlert}
          >
            Delete
          </Button>
          <Divider style={{ marginBottom: 20 }} />
        </SafeAreaView>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const EditLocationScreen = connect(mapStateToProps, { updateLocations })(
  _EditLocationScreen
);

export { EditLocationScreen };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
  },
  button: {
    width: "40%",
    alignSelf: "center",
    marginBottom: 20,
  },
  textfieldLocation: {
    marginTop: 5,
    margin: 20,
  },
  textfieldRadius: {
    flex: 1,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 50,
    marginEnd: 20,
    marginStart: 20,
  },
  radiusContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginStart: 20,
  },
  radiusText: {
    flex: 1,
    alignSelf: "flex-end",
    fontSize: 18,
    marginStart: 20,
    paddingBottom: 10,
  },
});
