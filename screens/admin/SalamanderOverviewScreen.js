import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
  ScrollView,
  Modal,
} from "react-native";
import CustomButton from "../../components/CustomButton";
import CustomDropDown from "../../components/CustomDropDown";
import ImageViewer from "react-native-image-zoom-viewer";
import SalamanderData from "../../components/SalamanderData";
import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import { toast500, toastError } from "../../constants/toasts";

const deviceWidth = Dimensions.get("window").width;

/**
 * This component renders the SalamanderOverviewScreen. This is the screen an administrator will be navigated to if they click on one of the 
 * cards in the ManageSalamandersScreen. Here, an overview of the salamander is shown, all images taken of the salamander, and metadata of the entry. 
 * If the admin wants to change one of the entries, they can choose one of the images/entries to edit it. They will then be navigated to the ChangeSalamanderScreen. 
 * 
 * @param {*} navigation - extracted from props, used for navigating to other screens. 
 * @param {*} route - extracted from props, used for getting data from the last screen. 
 * 
 * @returns the component. 
 */
const SalamanderOverviewScreen = ({ navigation, route }) => {
  const { salamanderData, getSalamanders } = route.params;

  const [isPreviewMode, setPreviewMode] = useState(false);
  const [images, setImages] = useState(null);
  const [image, setImage] = useState(null);
  const [imageNumberValue, setImageNumberValue] = useState(null);
  const [salamander, setSalamander] = useState({});
  const [showIndicator, setShowIndicator] = useState(false);

  const getImageNumberValue = (value) => {
    setImageNumberValue(value);
  };

  const getImageNumbers = () => {
    let imageNumbers = [];
    for (var i = 0; i < salamanderData.images.length; i++) {
      imageNumbers.push({
        label: (i + 1).toString(),
        value: i,
      });
    }
    return imageNumbers;
  };

  const imageNumber = getImageNumbers();

  const navigateToChangeSalamander = () => {
    setShowIndicator(true);
    APIKit.get(`/editSalamander/${salamander.id}/${imageNumberValue}`)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          navigation.navigate("Verify", {
            screen: "ChangeSalamander",
            params: {
              salamanderImageData: response.data,
              getSalamanders: getSalamanders,
            },
          });
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  const modal = () => {
    setImages(salamander.images);
    setPreviewMode(true);
  };

  useEffect(() => {
    setSalamander(salamanderData);
    setImage(salamanderData.images[0].url);
    setImageNumberValue(0);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <CustomActivityIndicator
          style={styles.activityIndicator}
          visible={showIndicator}
        />
        <Text style={{ marginTop: 20, marginStart: 20 }}>
            Number of salamanders: {imageNumber.length}
          </Text>
        <View style={styles.previewContainer}>
          <TouchableWithoutFeedback onPress={() => modal()}>
            <Image
              style={{
                height: 120,
                width: 320,
                resizeMode: "contain",
                alignSelf: "center",
                marginTop: 50,
              }}
              source={{ uri: image }}
            />
          </TouchableWithoutFeedback>
          {isPreviewMode && (
            <Modal
              visible={isPreviewMode}
              transparent={true}
              onRequestClose={() => {
                setPreviewMode(false);
              }}
            >
              <ImageViewer
                enableSwipeDown={true}
                onCancel={() => setPreviewMode(false)}
                imageUrls={images}
              />
            </Modal>
          )}
        </View>
        <SalamanderData
          id={salamander.id}
          location={salamander.location}
          sex={salamander.sex}
          species={salamander.species}
        />
        <Text style={{ marginTop: 20, marginStart: 20 }}>
          Choose image to edit:
        </Text>
        <View
          style={{
            ...(Platform.OS !== "android" && {
              zIndex: 10,
            }),
            width: "20%",
            justifyContent: "center",
            marginStart: 50,
            marginTop: 20,
          }}
        >
          <CustomDropDown
            defaultValue={0}
            myItems={imageNumber}
            sendDataToParent={getImageNumberValue}
          />
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            marginTop: 30,
            marginBottom: 60,
          }}
        >
          <CustomButton
            style={styles.button}
            title="Cancel"
            mode="outlined"
            onPress={() => navigation.goBack()}
          />
          <CustomButton
            style={styles.button}
            title="Edit"
            mode="contained"
            onPress={() => navigateToChangeSalamander()}
            disabled={imageNumberValue == null}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default SalamanderOverviewScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 25,
  },
  previewContainer: {
    alignSelf: "center",
    backgroundColor: "transparent",
    height: deviceWidth * 0.5,
    width: deviceWidth * 0.5,
    margin: 10,
  },
  button: {
    margin: 10,
    width: "40%",
  },
  activityIndicator: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: `rgba(0,0,0,${0.1})`,
  },
});
