import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Platform,
  SafeAreaView,
  ScrollView,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
  Alert,
  Modal,
  KeyboardAvoidingView
} from "react-native";
import CustomDropDown from "../../components/CustomDropDown";
import { TextInput, HelperText, Button, RadioButton, Colors } from "react-native-paper";
import { Divider } from "react-native-paper";
import ImageViewer from "react-native-image-zoom-viewer";
import {
  NUMBER_MAX_LENGTH,
  MEASUREMENT_REGEX,
  INVALID_MEASUREMENT_MESSAGE,
} from "../../constants/inputRequirements";
import APIKit from "../../APIkit";
import MatchResponse from "../../components/MatchResponse";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import { updateLocations, updateSalamanderList } from "../../redux";
import { connect } from "react-redux";
import CustomButton from "../../components/CustomButton";
import { toastSuccess, toastError, toast500, toastInfo } from "../../constants/toasts";
import CustomRadioButtonGroup from "../../components/CustomRadioButtonGroup";

const deviceWidth = Dimensions.get("window").width;

const species = [
  { label: "Northern Crested Newt", value: "northern_crested_newt" },
  { label: "Smooth Newt", value: "smooth_newt" },
];

const sexValues = {firstValue:"female", secondValue:"male", thirdValue:"juvenile", 
firstText:"Female", secondText:"Male", thirdText:"Juvenile"};

/**
 * This component renders the ChangeSalamanderScreen in the application. In this screen, and administrator can edit an individual entry of one salamander. 
 * The admin can edit location, species, sex, weight and length. If the user edits either location, species or sex, the salamander will be rematched
 * by the server, and will either get a match and adapt this salamanders ID, or no match, and a new salamander will be made with a new ID. 
 * 
 * This is the largest component, and it might be possible to refactor it, and make it shorter. 
 * 
 * @param {*} props - the userReducer, which contains the state of the user, is extracted from redux, as well as the updateSalamanderList. 
 *                    Like all the other components, props.navigation is used to navigate to screens. 
 * 
 * @returns the component
 */
const _ChangeSalamanderScreen = (props) => {
  const { userReducer, updateSalamanderList } = props;
  const { locationList } = userReducer;

  const { salamanderImageData } = props.route.params;

  const [isPreviewMode, setPreviewMode] = useState(false);
  const [images, setImages] = useState(null);
  const [image, setImage] = useState(null);
  const [weightHint, setWeightHint] = useState(false);
  const [lengthHint, setLengthHint] = useState(false);
  const [locationValue, setLocationValue] = useState(
    salamanderImageData.location
  );
  const [speciesValue, setSpeciesValue] = useState(salamanderImageData.species);
  const [sexValue, setSexValue] = useState(salamanderImageData.sex);
  const [showIndicator, setShowIndicator] = useState(false);
  const [visibleMatch, setVisibleMatch] = useState(false);
  const [response, setResponse] = useState({
    responseMatch: "",
    id: "",
    responseText: "",
    responseColor: "",
  });
  const [length, setLength] = useState(() => {
    if (salamanderImageData.length != null) {
      return salamanderImageData.length.toString();
    } else {
      return "";
    }
  });
  const [weight, setWeight] = useState(() => {
    if (salamanderImageData.weight != null) {
      return salamanderImageData.weight.toString();
    } else {
      return "";
    }
  });

  const checkWeight = (weight) => {
    setWeight(weight);
    setWeightHint(!weight.match(MEASUREMENT_REGEX) && weight !== "");
  };

  const checkLength = (length) => {
    setLength(length);
    setLengthHint(!length.match(MEASUREMENT_REGEX) && length !== "");
  };

  const deleteSalamanderEntry = () => {
    setShowIndicator(true);

    let bodyFormData = new FormData();
    bodyFormData.append("id", salamanderImageData.salamanderId);
    bodyFormData.append("image_id", salamanderImageData.imageId);
    bodyFormData.append("location", salamanderImageData.location);

    APIKit.delete("/editSalamander", { data: bodyFormData })
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          props.navigation.navigate("ManageSalamanders");
          let location = salamanderImageData.location;
          updateSalamanderList({ setShowIndicator, location });
          toastSuccess("top", 3000, response.data.message); 
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  const editSalamander = () => {
    setShowIndicator(true);
      let bodyFormData = new FormData();

      bodyFormData.append("id", salamanderImageData.salamanderId);
      bodyFormData.append("image_id", salamanderImageData.imageId);
      bodyFormData.append("location", salamanderImageData.location);
      bodyFormData.append("new_location", locationValue);
      bodyFormData.append("new_species", speciesValue);
      bodyFormData.append("new_sex", sexValue);
      bodyFormData.append("length", length);
      bodyFormData.append("weight", weight);

      APIKit.put("/editSalamander", bodyFormData)
      .then(function (response) {
        let locationValue = salamanderImageData.location;
        updateSalamanderList({ setShowIndicator, locationValue });
        setShowIndicator(false);
        if (response.data.status === 200) {
          if (response.data.matching === "Match!") {
            setResponse({
              responseMatch: response.data.matching,
              id: response.data.id,
              responseText: response.data.message,
              responseColor: Colors.green300,
            });
            setVisibleMatch(true);
          } else if (response.data.matching === "No match.") {
            setResponse({
              responseMatch: response.data.matching,
              id: response.data.id,
              responseText: response.data.message,
              responseColor: Colors.white,
            });
            setVisibleMatch(true);
          } else {
              props.navigation.navigate("ManageSalamanders");
              toastSuccess("top", 2000, response.data.message); 
            }
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  const createTwoButtonAlert = () =>
    Alert.alert(
      "Deleting Salamander",
      "Deleting this salamander will remove it from the system. Are you sure you will continue?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        { text: "OK", onPress: () => deleteSalamanderEntry() },
      ],
      { cancelable: false }
    );

  const getLocationValue = (value) => {
    setLocationValue(value);
  };

  const getSpeciesValue = (value) => {
    setSpeciesValue(value);
  };

  const getSexValue = (value) => {
    setSexValue(value);
  };

  const getLocations = () => {
    let locationNames = [];
    locationList.map((location) => {
      locationNames.push({
        label: location.name.charAt(0).toUpperCase() + location.name.slice(1),
        value: location.name,
      });
    });
    return locationNames;
  };

  const modal = () => {
    setImages(salamanderImageData.images);
    setPreviewMode(true);
  };

  const locations = getLocations();

  const keyboardProps = Platform.select({
    android: { behavior:'padding' },
    ios: { behavior:'position' },
  });

  useEffect(() => {
    setImage(salamanderImageData.images[0].url);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <KeyboardAvoidingView {...keyboardProps} keyboardVerticalOffset={30}>
          <CustomActivityIndicator
            style={styles.activityIndicator}
            visible={showIndicator}
          />
          <MatchResponse
            visible={visibleMatch}
            onCancel={() => props.navigation.navigate("ManageSalamanders")}
            responseMatch={response.responseMatch}
            id={response.id}
            responseText={response.responseText}
            responseColor={response.responseColor}
          />
          <Text style={{ marginStart: 20, fontSize: 18 }}>
            Salamander ID: {salamanderImageData.salamanderId}
          </Text>
          <View style={styles.previewContainer}>
            <TouchableWithoutFeedback onPress={() => modal()}>
              <Image
                source={{ uri: image }}
                style={{
                  flex: 1,
                  height: 120,
                  width: 320,
                  resizeMode: "contain",
                  alignSelf: "center",
                }}
              />
            </TouchableWithoutFeedback>
            {isPreviewMode && (
              <Modal
                visible={isPreviewMode}
                transparent={true}
                onRequestClose={() => {
                  setPreviewMode(false);
                }}
              >
                <ImageViewer
                  enableSwipeDown={true}
                  onCancel={() => setPreviewMode(false)}
                  imageUrls={images}
                />
              </Modal>
            )}
          </View>
          <Text style={styles.text}>Location</Text>
          <View
            style={{
              ...(Platform.OS !== "android" && {
                zIndex: 20,
              }),
              margin: 10,
            }}
          >
            <CustomDropDown
              defaultValue={salamanderImageData.location}
              myItems={locations}
              sendDataToParent={getLocationValue}
            />
          </View>
          <Text style={styles.text}>Species</Text>
          <View
            style={{
              ...(Platform.OS !== "android" && {
                zIndex: 15,
              }),
              margin: 10,
            }}
          >
            <CustomDropDown
              defaultValue={salamanderImageData.species}
              myItems={species}
              sendDataToParent={getSpeciesValue}
            />
          </View>
          <Text style={styles.text}>Sex</Text>
          <View style={{marginStart: 20}}>
            <CustomRadioButtonGroup 
              default={sexValue}
              values={sexValues}
              sendDataToParent={getSexValue}
            />
          </View>
          <View style={styles.textfieldContainer}>
            <View style={styles.textfield}>
              <TextInput
                onChangeText={(length) => checkLength(length)}
                value={length}
                keyboardType="decimal-pad"
                mode="flat"
                placeholder="Length"
                label="Length (Optional)"
                maxLength={NUMBER_MAX_LENGTH}
              />
              {lengthHint && (
                <HelperText type="error" visible={lengthHint}>
                  {INVALID_MEASUREMENT_MESSAGE}
                </HelperText>
              )}
            </View>
            <Text style={styles.textfieldText}>Millimeters</Text>
          </View>
          <View style={styles.textfieldContainer}>
            <View style={styles.textfield}>
              <TextInput
                onChangeText={(weight) => checkWeight(weight)}
                keyboardType="decimal-pad"
                value={weight}
                mode="flat"
                placeholder="Weight"
                label="Weight (Optional)"
                maxLength={NUMBER_MAX_LENGTH}
              />
              {weightHint && (
                <HelperText type="error" visible={weightHint}>
                  {INVALID_MEASUREMENT_MESSAGE}
                </HelperText>
              )}
            </View>
            <Text style={styles.textfieldText}>Gram</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              marginTop: 30,
              marginBottom: 20,
            }}
          >
            <CustomButton
              style={styles.button}
              title="Cancel"
              mode="outlined"
              onPress={() => props.navigation.goBack()}
            />
            <CustomButton
              style={styles.button}
              title="Confirm"
              mode="contained"
              disabled={
                !(
                  !weightHint &&
                  !lengthHint &&
                  locationValue != null &&
                  speciesValue != null &&
                  sexValue != null
                )
              }
              onPress={() => editSalamander()}
            />
          </View>
          <Divider style={{ marginTop: 20 }} />
          <Button
            style={styles.button}
            color="red"
            title="Delete"
            mode="contained"
            onPress={createTwoButtonAlert}
          >Delete</Button>
          <Divider style={{ marginBottom: 20 }} />
        </KeyboardAvoidingView>
      </ScrollView>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const ChangeSalamanderScreen = connect(mapStateToProps, {
  updateLocations,
  updateSalamanderList,
})(_ChangeSalamanderScreen);

export { ChangeSalamanderScreen };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  textfield: {
    marginTop: 15,
    width: "50%",
    alignSelf: "flex-start",
    marginStart: 20,
  },
  textfieldContainer: {
    flexDirection: "row",
    marginStart: 20,
  },
  textfieldText: {
    flex: 1,
    alignSelf: "flex-end",
    fontSize: 18,
    marginStart: 20,
    paddingBottom: 10,
  },
  text: {
    marginStart: 20,
  },
  previewContainer: {
    alignSelf: "center",
    backgroundColor: "transparent",
    height: deviceWidth * 0.5,
    width: deviceWidth * 0.5,
    margin: 20,
  },
  button: {
    margin: 10,
    width: "40%",
    alignSelf: "center",
  },
});
