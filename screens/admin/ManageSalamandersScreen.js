import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  FlatList,
  Platform,
} from "react-native";
import CustomDropDown from "../../components/CustomDropDown";
import {
  updateLocations,
  updateSalamanderList,
  removeSalamanderList,
} from "../../redux";
import { connect } from "react-redux";
import CustomButton from "../../components/CustomButton";
import ManageSalamanderCard from "../../components/ManageSalamanderCard";
import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import { toastError, toast500 } from "../../constants/toasts";

/**
 * This component renders the screen where an administrator can fetch salamanders from a given location, and look at metadata. 
 * It consists of a flatlist that renders cards that contains all salamanders at one location. See ManageSalamanderCard for more details. 
 * 
 * @param {*} props used for redux and navigation. 
 * 
 * @returns the component. 
 */
const _ManageSalamandersScreen = (props) => {
  const { userReducer, updateSalamanderList, removeSalamanderList } = props;
  const { locationList, salamanderList } = userReducer;

  const [locationValue, setLocationValue] = useState(null);
  const [showIndicator, setShowIndicator] = useState(false);

  const getLocationValue = (value) => {
    setLocationValue(value);
  };

  // To make the back-button on android behave correctly and not go to the VerifyPasswordScreen
  React.useEffect(() => {
    const unsubscribe = props.navigation.addListener("focus", () => {
      if (locationValue) {
        getSalamanders();
      }
    });
    props.navigation.addListener('beforeRemove', (e) => {
      e.preventDefault();
      props.navigation.navigate("Profile");
    });
    return unsubscribe;
  }, []);
  
  const getLocations = () => {
    let locationNames = [];
    locationList.map((location) => {
      locationNames.push({
        label: location.name.charAt(0).toUpperCase() + location.name.slice(1),
        value: location.name,
      });
    });
    return locationNames;
  };

  const locations = getLocations();

  const navigateToSalamanderOverview = (id) => {
    setShowIndicator(true);
    APIKit.get(`/salamander/${id}`)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          props.navigation.navigate("Verify", {
            screen: "SalamanderOverview",
            params: {
              salamanderData: response.data,
            },
          });
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  const renderItem = ({ item }) => (
    <ManageSalamanderCard
      title={item.id}
      date={item.date}
      species={item.species}
      sex={item.sex}
      individuals={item.individuals}
      navigateToSalamanderOverview={navigateToSalamanderOverview}
    />
  );

  const getSalamanders = () => {
    updateSalamanderList({ setShowIndicator, locationValue });
  };

  return (
    <SafeAreaView style={styles.container}>
      <CustomActivityIndicator
        style={styles.activityIndicator}
        visible={showIndicator}
      />
      <Text style={styles.text}>Location</Text>
      <View
        style={{
          ...(Platform.OS !== "android" && {
            zIndex: 10,
          }),
          flexDirection: "row",
        }}
      >
        <View style={{ width: "60%", margin: 10 }}>
          <CustomDropDown
            myItems={locations}
            sendDataToParent={getLocationValue}
          />
        </View>
        <CustomButton
          style={styles.buttonFetch}
          title="Search"
          mode="contained"
          onPress={() => getSalamanders()}
          disabled={locationValue == null}
          icon="magnify"
        />
      </View>
      <FlatList
        data={salamanderList}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
      <CustomButton
        style={styles.buttonClose}
        title="Done"
        mode="outlined"
        onPress={() => {
          removeSalamanderList();
          props.navigation.navigate("Profile");
        }}
      />
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => ({
  userReducer: state.userReducer,
});

const ManageSalamandersScreen = connect(mapStateToProps, {
  updateSalamanderList,
  updateLocations,
  removeSalamanderList,
})(_ManageSalamandersScreen);

export { ManageSalamandersScreen };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    marginTop: 30,
  },
  buttonFetch: {
    width: "30%",
  },
  buttonClose: {
    width: "40%",
    marginBottom: 25,
    marginTop: 10,
  },
  text: {
    marginStart: 20,
  },
  activityIndicator: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: `rgba(0,0,0,${0.1})`,
  },
});
