import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  FlatList,
  SafeAreaView,
  Text
} from "react-native";

import CustomButton from "../../components/CustomButton";
import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import PendingUser from "../../components/PendingUser";
import { toast500, toastError } from "../../constants/toasts";
import { Colors } from "react-native-paper"
 
/**
 * This component renders the screen where an administrator can look at pending user, who are waiting for access to the system. 
 * To access this screen, The admin has to re-authenticate, as it is about handling sensitive data.
 * The admin can either accept or deny a user. If the admin denies a users access, the user will be deleted from the system, and has to make a new account
 * at a later date. If the admin accepts a user, the user will gain access to all non-administrative features. The user can now be seen in manageUsersScreen. 
 * It consists of a list containing pending users in the system, displaying their emails and a button for denying access and a button for granting access. 
 * See ManageUser for more details. 
 * 
 * @param {*} navigation - extracted from props, used for navigating to other screens. 
 * 
 * @returns the component.
 */
const PendingUsersScreen = ({ navigation }) => {
  const [pendingUsers, setPendingUsers] = useState([]);
  const [showIndicator, setShowIndicator] = useState(false);

  const renderItem = ({ item }) => (
    <PendingUser
      email={item.email}
      id={item.id}
      getPendingUsers={getPendingUsers}
    />
  );

  const getPendingUsers = () => {
    setShowIndicator(true);
    APIKit.get("/pendingUsers")
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          setPendingUsers(response.data.users);
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  useEffect(() => {
    getPendingUsers();
    navigation.addListener('beforeRemove', (e) => {
      e.preventDefault();
      navigation.navigate("Profile");
    });

  }, []);

  return (
    <SafeAreaView style={{ flex: 1, alignItems: "stretch" }}>
      <CustomActivityIndicator style={styles.activityIndicator} visible={showIndicator} />
      {pendingUsers.length === 0 && (
            <View style={styles.textContainer}>
              <Text style={styles.text}>There are currently no pending users.</Text>
            </View>
          )}
      <FlatList
        data={pendingUsers}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
      <View style={{ flex: 1 }}>
        <CustomButton
          style={styles.button}
          mode="outlined"
          title="Done"
          onPress={() => navigation.navigate("Profile")}
        />
      </View>
    </SafeAreaView>
  );
};

export default PendingUsersScreen;

const styles = StyleSheet.create({
  button: {
    width: "40%",
    alignSelf: "center",
    marginTop: 20,
  },
  textContainer: {
    marginTop: 50, 
    alignSelf:"center"
  },
  text: {
    fontSize: 16, 
    color: Colors.grey700 
  }
});
