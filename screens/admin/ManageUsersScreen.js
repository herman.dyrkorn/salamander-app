import React, { useState, useEffect } from "react";
import { StyleSheet, View, SafeAreaView, FlatList, Text } from "react-native";

import CustomButton from "../../components/CustomButton";
import APIKit from "../../APIkit";
import CustomActivityIndicator from "../../components/CustomActivityIndicator";
import ManageUser from "../../components/ManageUser";
import { toastError, toast500 } from "../../constants/toasts";
import { Colors } from "react-native-paper"

/**
 * This component renders the screen where an administrator can manage accepted users in the system. To access this screen, 
 * The admin has to re-authenticate, as it is about handling sensitive data. The admin can revoke users access, 
 * make users admin or remove admin privileges. It consists of a list containing users in the system, displaying their emails and an edit button. 
 * See ManageUser for more details. 
 * 
 * @param {*} navigation - extracted from props, used for navigating to other screens. 
 *  
 * @returns the component. 
 */
const ManageUsersScreen = ({ navigation }) => {
  const [users, setUsers] = useState([]);
  const [showIndicator, setShowIndicator] = useState(false);

  const renderItem = ({ item }) => (
    <ManageUser
      email={item.email}
      id={item.id}
      admin={item.admin}
      accepted={item.accepted}
      getUsers={getUsers}
    />
  );

  const getUsers = () => {
    setShowIndicator(true); 
    APIKit.get("/manageUsers")
      .then(function (response) {
        setShowIndicator(false); 
        if (response.data.status === 200) {
          setUsers(response.data.users);
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  // To make the back-button on android behave correctly, and not go to the VerifyPasswordScreen
  useEffect(() => {
    getUsers();
    navigation.addListener('beforeRemove', (e) => {
      e.preventDefault();
      navigation.navigate("Profile");
    });
  }, []);

  return (
    <SafeAreaView style={{ flex: 1, alignItems: "stretch", marginTop: 15 }}>
      <CustomActivityIndicator style={styles.activityIndicator} visible={showIndicator} />
      {users.length === 0 && (
            <View style={styles.textContainer}>
              <Text style={styles.text}>There are currently no users to manage.</Text>
            </View>
          )}
      <FlatList
        data={users}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
      <View style={{ flex: 1 }}>
        <CustomButton
          style={styles.button}
          mode="outlined"
          title="Done"
          onPress={() => navigation.navigate("Profile")}
        />
      </View>
    </SafeAreaView>
  );
};

export default ManageUsersScreen;

const styles = StyleSheet.create({
  button: {
    width: "40%",
    alignSelf: "center",
    marginTop: 20,
  },
  textContainer: {
    marginTop: 50, 
    alignSelf:"center"
  },
  text: {
    fontSize: 16, 
    color: Colors.grey700 
  }
});
