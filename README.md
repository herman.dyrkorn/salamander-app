# Salamander Identification Application - mobile application

<p align="center">
    <img src="images/Salamander_logocircle.png" alt="salamander" width="300"/>
</p>

## Contents

- [Introduction](#introduction)
- [Development Environment](#development-environment)
- [System](#system)
- [Build](#build)

## Introduction

This will serve as the documentation for the mobile application part of the Salamander Identification Application system, developed during the Spring of 2021 as a Bachelor's project proposed by the Norwegian Institute of Nature Research (NINA). The application is meant to aid the researchers at NINA in estimating the populations of Northern crested newts and Smooth newts. 

### Developers: 

* Andrea Magnussen
* Anders Sundstedt Langlie
* Herman Andersen Dyrkorn
* Eirik Martin Danielsen

## Devlopment Environment

To be able to run the mobile application in a development environment, several steps are required. The expo documentation can be found [**here**](https://docs.expo.io/). 

### Setup

1. Expo CLI needs to be installed: ```npm install -g expo-cli```
2. Clone the project where you want the project to be located on your computer: ```git clone <link>```
3. Run ```npm i``` in the project folder to install all required libraries. **NOTE**: At the time being, react-native version 0.64.2 >= does not seem to work on android, make sure to change the version to 0.63.4 if there are problems after doing an npm i or npm audit fix. 
4. To run the expo server, run ```expo start``` in the project folder. A QR-code should now be visible. 
5. Download the Expo Go app, and scan the QR-code. The salamander app should now be visible in the Expo app. 
6. Make sure a server is running, and that the IP-address in the APIkit.js file is correct. If the server is running, it should be possible to make requests from the application. 

## System

In this section, we will go through the framework and libraries used, as well as how the application work and what it consists of. 

The application is currently a thin client, and relies on the server to get data. The image below shows a visualization of the system architecture. 

<p align="center">
    <img src="images/architecture.png" alt="architecture" width="600"/>
</p>

### Framework and Libraries

The frontend is written in the Javascript framework developed by facebook, React Native. We used several additional libraries to make this application. 

#### React Native

The documentation for React Native can be found [**here**](https://reactnative.dev/docs/getting-started). We decided to use functional components while developing the application. For further development, we recommend to continue using this way of adding/developing new components. 

#### Libraries

* **React Navigation**: Used for handling navigation in the application. Documentation can be found [**here**](https://reactnavigation.org/docs/getting-started/).
* **React Native Paper**: Used for styling of the components. Documentation can be found [**here**](https://callstack.github.io/react-native-paper/).
* **React Native Maps**: Used for the map feature in the application. Documentation can be found [**here**](https://github.com/react-native-maps/react-native-maps).
* **React Native Dropdown Picker**: Used for the dropdowns in the application. Documentation can be found [**here**](https://hossein-zare.github.io/react-native-dropdown-picker-website/docs/).
* **React Native toast message**: Used for the toast messages in the application. The documentation can be found [**here**](https://github.com/calintamas/react-native-toast-message).
* **React Redux**: Used for state management in the application. The documentation can be found [**here**](https://react-redux.js.org/introduction/getting-started).
* **Axios**: Axios is used for handling network requests in the application. The documentation can be found [**here**](https://www.npmjs.com/package/axios).
* **Expo**: Expo is used for the development environment. It was used during development, and can run/build the application while developing. The documentation can be found [**here**](https://docs.expo.io/).
* **Expo Camera**: Used for the built-in camera feature. The documentation can be found [**here**](https://docs.expo.io/versions/latest/sdk/camera/).
* **Expo Image Picker**: Used to be able to upload images from the phones library. The documentation can be found [**here**](https://docs.expo.io/versions/latest/sdk/imagepicker/).
* **Expo Image Manipulator**: Used to resize the image before upload, so it will take less time. The documentation can be found [**here**](https://docs.expo.io/versions/latest/sdk/imagemanipulator/).

### Modules

We divided the frontend into roughly four modules; home, camera, profile and admin. This was based on the bottom tab navigation routes, in addition to the extra screens that an admin has access to. Every module have several features each. 

#### Home Module

<p align="center">
    <img src="images/home.png" alt="home" width="200"/>
    <img src="images/register_location.png" alt="register location" width="200"/>
    <img src="images/edit_location.png" alt="edit location" width="200"/>
</p>

The home screen is the first a user sees when they log into the application. A map containing all registered locations are displayed, including the user's current position if they have allowed the application to use it. If the user long press on the map, a new marker will be added and a button to add a new location will appear. 

When a user wants to add a new location, they will be taken to a modal screen where they can give the new location a name and a radius. The radius is simply used for aesthetic purposes to highlight the area covered. When the location is registered, the user will be taken back to the home screen, and the newly created location will show on the map. 

The user can also edit existing locations by clicking on the locations, and the "edit location" button. They will be taken to a modal screen where they can edit the name and the radius. If there are no registered salamanders at the given location, they can also delete the location. 

#### Camera Module

<p align="center">
    <img src="images/camera.png" alt="camera" width="200"/>
    <img src="images/processing.png" alt="processing" width="200"/>
    <img src="images/register_salamander.png" alt="register salamander" width="200"/>
</p>

The camera screen is the second destination on the bottom tab navigator. The screen consists of two buttons, one where the user can upload an image from their phones gallary, and one where the user can take a new picture while using the application. When a picture is uploaded, the user can start processing the image. When an image is being processed, an activity indicator will appear. The algorithm will try to isolate the abdominal pattern and straighten the image, and return this to the user for approval. This process may take some time depending on the internet speed (approximately 15-50 seconds).   

When the processing is completed, the user will be redirected to a modal screen where they can look at the processed image. If the pattern looks good, the user can start to fill in data about the salamander. The mandatory data includes location, species and sex, and the optional data includes weight and length. It is important that the mandatory data is correct, as the algorithm will only try to match against salamanders with the same location, species and sex. When the data is provided, the user can click on the "confirm"-button.

<p align="center">
    <img src="images/match.png" alt="match" width="200"/>
    <img src="images/no_match.png" alt="no match" width="200"/>
</p>

If there is a match, a green popup with information about which salamander it was matched against will appear. If there were no match, a new salamander with a new ID will be registered in the database, and the user will be notified through a white popup. When the user clicks "ok" on the popup, they will be redirected to the camera screen, and can process a new image.  

#### Profile Module

<p align="center">
    <img src="images/profile.png" alt="match" width="200"/>
    <img src="images/verify_password.png" alt="no match" width="200"/>
</p>

The profile screen is the third, and currently final bottom tab destination, located on the bottom right side of the mobile application. Here, a user can manage their own data like their name (only currently used for aesthetic purposes), their email, their password and deletion of their user. If the user wants to access critical features (like email, password and deletion of the account) they have to re-authenticate through a modal where they need to verify their password. 

#### Admin Module

<p align="center">
    <img src="images/profile.png" alt="match" width="200"/>
</p>

The features of this module are all located on the profile screen, but only visible to users with the role 'administrator'. The admin specific features include being able to manage pending users (new users who wants access to the system), manage all users in the system, and the ability to manage all registered salamanders in the system. 

<p align="center">
    <img src="images/pending_users.png" alt="pending users" width="200"/>
    <img src="images/manage_users.png" alt="manage users" width="200"/>
    <img src="images/change_rights.png" alt="change rights" width="200"/>
</p>

The user management features, consists of two screens, one for pending users and one for all the current users in the system. Pending users consists of a list with the users emails and two corresponding buttons, one for granting the user's access, and one for denying them. If the admin denies a pendning user's access, they will be completely deleted from the system. If an admin accepts a user, they will be moved to the manage users screen, and the admin can further manage them there. 

The manage user screen consists of a list with the users emails and one corresponding button named 'edit'. If the admin clicks on this, a dialog will show, where the admin can change the user's role. They can either make them admin, remove their admin privileges or deny them access. If the user is denied access, they are moved back to the pending users screen, and will have to be accepted again to be able to log into the application. The admin can also reset a user's password. By resetting a user's password, the administrator will recieve the new password for that user. This password will then have to be sent to the user for login, and can be changed after the user logs into the system again.

Currently, to avoid the scenario of no administrators in the system, the only option to delete an admin account, is if another admin in the system does it. This is done either by removing the admin privilege of the user, so that they can delete their own account. Or, by denying the user access to the system, and delete them from the pending user list. This way, there will always be at least one admin in the system. 

<p align="center">
    <img src="images/manage_salamanders.png" alt="manage salamanders" width="200"/>
    <img src="images/manage_salamanders_cards.png" alt="manage salamanders cards" width="200"/>
    <img src="images/salamander_overview.png" alt="salamander overview" width="200"/>
    <img src="images/change_salamander.jpeg" alt="change salamander" width="200"/>
</p>

The last feature an admin has access is the manage salamander feature. Here, an admin can look at all registered salamanders at a given location. They can look at every individual salamander, and all entries registered for that salamander. If they want to change a salamanders data, due to either the data not being correct, or they want to update its measurements, this is possible by choosing the entry they want to edit, and go to the change salamander screen. If the user changes either location, species or sex, the salamander will be rematched with the new data. The same matching process as in the register salamander screen in the camera module will be done. 

## Build

To build the application, we followed the official [**expo guide**](https://docs.expo.io/distribution/building-standalone-apps/). It is important to note that we have currently only tried building for android, and not iOS, as an iOS developer account is needed to do this.

Building the app can be done using Expo's built-in build tool. To use the map, a Google API key is needed. Read the [**official documentation**](https://developers.google.com/maps/documentation/javascript/get-api-key) to learn how to create and manage Google API keys. This key has to be placed inside the ```App.js``` file before building. Read [**Expo's guide**](https://docs.expo.io/versions/latest/sdk/map-view/) to learn how to include your key in the project.
