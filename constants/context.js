import React from "react";

/**
 * Used for remembering if the user is logged in or not. It was a early way of doing it, it might be better to
 * stick with using redux instead to handle which screens should be visible based on user status. 
 */

export const AuthContext = React.createContext();
