/**
 * These constants are used for the restrictions and regexes throughout the application
 */

export const NAME_MAX_LENGTH = 40;
export const NUMBER_MAX_LENGTH = 15;
export const LOCATION_NUMBER_MAX_LENGTH = 4;

/*******************************************  REGEXES  ************************************************/
export const EMAIL_REGEX =
  "^[ÆØÅæøåa-zA-Z0-9_.+-]+@[ÆØÅæøåa-zA-Z0-9-]+\\.[ÆØÅæøåa-zA-Z0-9-.]+$"; // Valid email
export const PASSWORD_REGEX =
  "^(?=.*[ÆØÅæøåA-Za-z])(?=.*\\d)[ÆØÅæøåA-Za-z\\d]{9,}$"; // 9 characters, at least one letter and one number
export const MEASUREMENT_REGEX = "^(\\d+(?:[\\.\\,]\\d{1,2})?)$"; // Accepts integers and decimal numbers with comma or period. 2 decimal precision!
export const INTEGER_REGEX = "^[0-9]*[1-9][0-9]*$"; //Accepts only integers
export const LEGAL_LOCATION_REGEX = "^[ÆØÅæøå \\w\\-]+$"; //Accepts trings containing legal file characters

/**************************************** FEEDBACK MESSAGES *****************************************/
export const INVALID_EMAIL_MESSAGE = "Invalid email!";
export const INVALID_PASSWORD_MESSAGE =
  "Password must be at least 9 characters long and contain a letter and a number!";
export const INVALID_MEASUREMENT_MESSAGE = "Invalid measurement!";
export const INVALID_INTEGER_MESSAGE = "Value is not an integer!";
export const NON_MATCHING_PASSWORDS_MESSAGE = "Passwords do not match!";
export const ILLEGAL_LOCATION_MESSAGE = "Illegal location name!";
