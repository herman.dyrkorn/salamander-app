/**
 * This file includes various toasts the user can get. More documentation on this: https://www.npmjs.com/package/react-native-toast-message
 */

import Toast from "react-native-toast-message";

/**
 * Shows a toast/response message to the user that everything went well (statuscode 200). 
 * 
 * @param {String} pos - position of the toast
 * @param {Integer} time - how long the toast should be visible
 * @param {String} message - displays information to the user. 
 */
export const toastSuccess = (pos, time, message) => {
    Toast.show({
        type: "success",
        text1: "Success",
        position: pos, 
        visibilityTime: time,
        text2: message,
      });
}

/**
 * Shows a toast/response message to the user that something went wrong (statuscode 4XX). 
 * 
 * @param {String} pos - position of the toast
 * @param {Integer} time - how long the toast should be visible
 * @param {String} message - displays information to the user. 
 */
export const toastError = (pos, time, message) => {
    Toast.show({
        type: "error",
        text1: "Error",
        position: pos,
        visibilityTime: time,
        text2: message,
      });
}

/**
 * Shows a toast/response message that provides information to the user. 
 * 
 * @param {String} pos - position of the toast
 * @param {Integer} time - how long the toast should be visible
 * @param {String} message - displays information to the user. 
 */
export const toastInfo = (pos, time, message) => {
    Toast.show({
        type: "info",
        text1: "Info",
        position: pos,
        visibilityTime: time,
        text2: message,
      });
}

/**
 * Will be displayed if the server is not responding/it failed before reaching the server. 
 * 
 * @param {Integer} statusCode - will provide different messages based on numerous status codes. 
 */
export const toast500 = (statusCode) => {

    let errorText = ""; 

    switch(statusCode) {
        case 401:
            errorText = "Token has expired, please log out."; break;
        case 429:
            errorText = "Too many request, please wait."; break; 
        case 500:
            errorText = "Internal server error."; break; 
        default:
            errorText = "System timed out."; break; 
        }

    Toast.show({
        type: "error",
        text1: "Error",
        text2: errorText
    });
    
}
