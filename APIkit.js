import axios from "axios";

/**
 * An instance of axios which is used everytime a network request is needed.
 * @baseURL where the request should be sent to.
 * @timeout how long the system will try to perform the request.
 * @headers what type of data is accepted.
 *
 * @return the instance.
 */
let APIKit = axios.create({
  baseURL: "IP",
  timeout: 60000,
  headers: { "Content-Type": "multipart/form-data" },
});

export default APIKit;
