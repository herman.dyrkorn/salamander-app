import APIKit from "../../APIkit";
import {
  USER_LOGIN,
  USER_LOGOUT,
  EDIT_NAME,
  EDIT_EMAIL,
  ON_ERROR,
  UPDATE_LOCATIONS,
  ADD_LAST_LOCATION,
  UPDATE_SALAMANDER_LIST,
  REMOVE_SALAMANDER_LIST,
} from "../actionConstants";
import { toast500, toastError, toastSuccess } from "../../constants/toasts";

/**
 * Signs user in to the application.
 *
 * @param {String} email: the users email.
 * @param {String} String: the users password.
 * @param {Function} SignInApp: Function to handle log in logic for the app
 * @returns async dispatch from redux
 */
export const onUserLogin = ({
  email,
  password,
  signInApp,
  setShowIndicator,
}) => {
  return async (dispatch) => {
    try {
      let bodyFormData = new FormData();
      bodyFormData.append("email", email);
      bodyFormData.append("password", password);
      setShowIndicator(true);

      const response = await APIKit.post("/login", bodyFormData);
      if (response.data.status === 200) {
        APIKit.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${response.data.access_token}`;
        setShowIndicator(false);
        signInApp(response.data.access_token);
        toastSuccess("top", 1000, response.data.message); 
        dispatch({ type: USER_LOGIN, payload: response.data });
      } else {
        setShowIndicator(false);
        toastError("top", 3000, response.data.message); 
      }
    } catch (error) {
      setShowIndicator(false);
      toast500(error.response.status); 
      dispatch({ type: ON_ERROR, payload: error });
    }
  };
};

/**
 * Signs user out of the application.
 *
 * @param {Function} signOutApp: Function to handle log out logic for the app
 * @returns  async dispatch from redux
 */
export const onUserSignOut = ({ signOutApp }) => {
  return async (dispatch) => {
    try {
      APIKit.defaults.headers.common["Authorization"] = undefined;
      signOutApp();
      dispatch({ type: USER_LOGOUT });
    } catch (error) {
      toast500(error.response.status); 
      dispatch({ type: ON_ERROR, payload: error });
    }
  };
};

/**
 * Updates locations in the app
 *
 * @returns async dispatch from redux
 */
export const updateLocations = ({ setShowIndicator }) => {
  return async (dispatch) => {
    try {
      setShowIndicator(true);
      const response = await APIKit.get("/location");
      if (response.data.status === 200) {
        setShowIndicator(false);
        dispatch({ type: UPDATE_LOCATIONS, payload: response.data.locations });
      } else {
        setShowIndicator(false);
      }
    } catch (error) {
      console.log(error);
      setShowIndicator(false);
      toast500(error.response.status); 
    }
  };
};

/**
 * To edit the users name in the application and to change the state.
 *
 * @param {*} props conatains the name to be edited
 * @returns async dispatch from redux
 */
export const onEditName = (props) => {
  return async (dispatch) => {
    try {
      dispatch({ type: EDIT_NAME, payload: props.name });
    } catch (error) {
      dispatch({ type: ON_ERROR, payload: error });
    }
  };
};

/**
 *
 * @param {String} locationValue contains value of the last location selected
 * @returns async dispatch from redux
 */
export const addLastLocation = ({ locationValue }) => {
  return async (dispatch) => {
    try {
      dispatch({ type: ADD_LAST_LOCATION, payload: locationValue });
    } catch (error) {
      dispatch({ type: ON_ERROR, payload: error });
    }
  };
};

/**
 * To edit the users email in the application and to change the state.
 *
 * @param {*} props conatains the email to be edited
 * @returns async dispatch from redux
 */
export const onEditEmail = (props) => {
  return async (dispatch) => {
    try {
      dispatch({ type: EDIT_EMAIL, payload: props.email });
    } catch (error) {
      dispatch({ type: ON_ERROR, payload: error });
    }
  };
};

/**
 * Updates the list of salamanders in manage salamanders after an action is performed
 *
 * @param {Bool} setShowIndicator handles the spinner logic
 * @param {String} locationValue contains the value of the current location where the user is managing salamanders
 * @returns async dispatch from redux
 */
export const updateSalamanderList = ({ setShowIndicator, locationValue }) => {
  return async (dispatch) => {
    try {
        setShowIndicator(true);
        const response = await APIKit.get(`/salamanders/${locationValue}`);
        if (response.data.status === 200) {
          setShowIndicator(false);
          dispatch({
            type: UPDATE_SALAMANDER_LIST,
            payload: response.data.salamanders,
          });
        } else {
          setShowIndicator(false);
          dispatch({ type: UPDATE_SALAMANDER_LIST, payload: [] });
        }
    } catch (error) {
      setShowIndicator(false);
      toast500(error.response.status); 
    }
  };
};

/**
 * Removes the salamander list in memory when done with managing salamanders to reset the feature.
 * 
 * @returns async dispatch from redux
 */
export const removeSalamanderList = (props) => {
  return async (dispatch) => {
    try {
      dispatch({ type: REMOVE_SALAMANDER_LIST, payload: [] });
    } catch (error) {
      dispatch({ type: ON_ERROR, payload: error });
    }
  };
};
