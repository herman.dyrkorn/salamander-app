import {
    USER_LOGIN,
    USER_LOGOUT,
    ON_ERROR,
    EDIT_NAME,
    EDIT_EMAIL,
    UPDATE_LOCATIONS,
    ADD_LAST_LOCATION,
    UPDATE_SALAMANDER_LIST,
    REMOVE_SALAMANDER_LIST,
} from '../actionConstants';

const INITIAL_STATE = {
    user: {},
    locationList: [],
    lastLocation: null,
    salamanderList: []
}

/**
 * Switch that overrides the old state with the new state depending on which action was triggered. 
 * 
 * @param {*} state the current state
 * @param {*} action the action from the actions in UserActions.js 
 * 
 * @returns the component
 */
export const userReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case USER_LOGIN:
            return {
                ...state,
                user: action.payload
            };
        case USER_LOGOUT:
            return {
                user: {},
                locationList: []
            };
        case EDIT_EMAIL:
            return {
                ...state,
                user: {
                    ...state.user,
                    email: action.payload
                }
            };
        case EDIT_NAME:
            return {
                ...state,
                user: {
                    ...state.user,
                    name: action.payload
                }
            };
        case ADD_LAST_LOCATION:
            return {
                ...state,
                lastLocation: action.payload
            };
        case UPDATE_LOCATIONS:
            return {
                ...state,
                locationList: action.payload
            };
        case UPDATE_SALAMANDER_LIST:
            return {
                ...state,
                salamanderList: action.payload
            };
        case REMOVE_SALAMANDER_LIST:
            return {
                ...state,
                salamanderList: action.payload
            };
        case ON_ERROR:
            return {
                ...state,
                appError: action.payload
            };
        default:
            return state;
    }
};
