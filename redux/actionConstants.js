// All actions used in redux

export const USER_LOGIN = 'DO_LOGIN';
export const USER_LOGOUT = 'DO_LOGOUT';
export const ON_ERROR = 'ON_ERROR';
export const EDIT_NAME = 'EDIT_NAME';
export const EDIT_EMAIL = 'EDIT_EMAIL';
export const UPDATE_LOCATIONS = 'UPDATE_LOCATION';
export const ADD_LAST_LOCATION = 'ADD_LAST_LOCATION';
export const UPDATE_SALAMANDER_LIST = 'UPDATE_SALAMANDER_LIST';
export const REMOVE_SALAMANDER_LIST = 'REMOVE_SALAMANDER_LIST';

