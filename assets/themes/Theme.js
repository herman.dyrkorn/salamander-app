import { DefaultTheme } from "react-native-paper";

/**
 * Defines the overall color theme of the application. See https://callstack.github.io/react-native-paper/theming.html for more documentation. 
 */

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: "orange",
    accent: "blue",
    background: "white", 
    text: "black",
    disabled: "#e2910f", 

    /*
    surface: "white", 
    placeholder: "lightgrey", 
    backdrop: "yellow"
    */
  },
};

export default theme;