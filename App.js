import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { Provider as PaperProvider } from "react-native-paper";
import { Provider, Provider as ReduxProvider } from "react-redux";

import { AuthContext } from "./constants/context";
import { store } from "./redux";
import theme from "./assets/themes/Theme";
import RootStackScreen from "./navigation/RootStackScreen";
import Toast from "react-native-toast-message";

/**
 * The application. 
 * 
 * @store - used for redux and the state store. 
 * @theme - used for theming and the look of the application (react native paper)
 * @authContext - the first way of handling the state of the user. Still in use to switch the stack based on the users
 *                log in state. Should be replaced by redux. 
 * @userToken - used for handling the user state.
 * @Toast - used for the toast messages to be visible on all screens when used. 
 * 
 * @returns the application. 
 */
export default function App() {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);

  /**
   * An early version of handling state in the application, to see if a user is logged in or not. 
   * Should be replaced by redux in the future. 
   */
  const authContext = React.useMemo(() => {
    return {
      signInApp: () => {
        setIsLoading(false);
        setUserToken(true);
      },
      signOutApp: () => {
        setIsLoading(false);
        setUserToken(null);
      },
    };
  }, []);

  return (
    <ReduxProvider store={store}>
      <PaperProvider theme={theme}>
        <AuthContext.Provider value={authContext}>
          <NavigationContainer>
            <RootStackScreen userToken={userToken} />
          </NavigationContainer>
        </AuthContext.Provider>
        <Toast ref={(ref) => Toast.setRef(ref)} />
      </PaperProvider>
    </ReduxProvider>
  );
}
