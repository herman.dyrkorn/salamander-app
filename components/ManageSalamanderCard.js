import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Card, Paragraph } from "react-native-paper";
import { Icon } from "react-native-elements";

/**
 * This component styles the salamander entry cards that are fetched when getting all salamanders from one location
 * in the ManageSalamandersScreen. 
 * 
 * @param {Function} props.navigateToSalamanderOverview(...) - navigates to the correct screen when clicked on
 * @param {String} props.title - contains the ID, is also passed to the salamanderOverviewScreen
 * @param {Integer} props.individual - number of images for a salamander. 
 * @param {String} props.species - what species the salamander is
 * @param {String} props.sex - what sex the salamander is
 * @param {Date} props.date - the date the last saved image is taken. 
 * 
 * @returns the component
 */

const ManageSalamanderCard = (props) => {
  return (
    <TouchableOpacity
      onPress={() => props.navigateToSalamanderOverview(props.title)}
    >
      <Card style={styles.card}>
        <View style={{ flexDirection: "row" }}>
          <View style={{ width: "90%" }}>
            <Card.Title title={"ID: " + props.title} />
            <Card.Content>
              <Paragraph>Number of images: {props.individuals.toString()}</Paragraph>
              <Paragraph>Species: {props.species}</Paragraph>
              <Paragraph>Sex: {props.sex}</Paragraph>
              <Paragraph>Date: {props.date}</Paragraph>
            </Card.Content>
          </View>
          <View style={styles.icon}>
            <Icon name={"chevron-right"} />
          </View>
        </View>
      </Card>
    </TouchableOpacity>
  );
};

export default ManageSalamanderCard;

const styles = StyleSheet.create({
  card: {
    margin: 10,
    paddingBottom: 15
  },
  icon: {
    paddingTop: 15,
    alignSelf: "center"
  },
});
