import React, { useState } from "react";
import { StyleSheet, View, Alert } from "react-native";
import { Button, Paragraph, Dialog, Portal, Text, Divider } from "react-native-paper";
import APIKit from "../APIkit";
import CustomActivityIndicator from "./CustomActivityIndicator";
import { toast500, toastError } from "../constants/toasts";
import CustomRadioButtonGroup from "./CustomRadioButtonGroup";

/**
 * This component renders a dialog, where the user can choose between different options. In this case, it is used for managing user privileges. 
 * 
 * @param {String} props.email - the email of the user that is currently being viewed
 * @param {String} props.id - the ID of the user that is currently being viewed
 * @param {Boolean} props.admin - false/true if admin or not
 * @param {Boolean} props.accepted - false/true if the user is accepted or not
 * @param {Function} props.onCancel - dismisses the dialog
 * @param {Function} props.getUsers - to update the user-list after changes are submitted
 * @param {Function} props.showFeedback - triggers the toast message when changes are submitted
 * 
 * @returns the component
 */

const CustomDialog = (props) => {
  const [value, setValue] = React.useState(null);
  const [showIndicator, setShowIndicator] = useState(false);
  const [resetCode, setResetCode] = useState(null);

  const values = {firstValue: "denyAccess", secondValue: "makeAdmin", thirdValue: "removeAdmin", 
  firstText:"Deny Access", secondText:"Make Admin", thirdText:"Remove Admin"};

  const getValue = (value) => {
    setValue(value);
  };

  const updateUser = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("email", props.email);
    bodyFormData.append("id", props.id);
    bodyFormData.append(value, true);

    setShowIndicator(true);

    APIKit.post("/manageUsers", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          props.showFeedback(response.data.message);
          props.getUsers();
          props.onCancel();
        } else {
          toastError("top", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  const resetPassword = () => {
    let bodyFormData = new FormData();
    bodyFormData.append("id", props.id);

    setShowIndicator(true);

    APIKit.put("/manageUsers", bodyFormData)
      .then(function (response) {
        setShowIndicator(false);
        if (response.data.status === 200) {
          props.showFeedback(response.data.message);
          setResetCode(response.data.new_password);
        }
      })
      .catch(function (response) {
        setShowIndicator(false);
        toast500(response.status); 
      });
  };

  const createResetAlert = () =>
    Alert.alert(
      "Resetting password",
      "This is a temporary password!\n\nPlease have the affected user change their password after login!",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        { text: "OK", onPress: () => resetPassword() },
      ],
      { cancelable: false }
    );

  return (
    <View>
      <Portal>
        <Dialog visible={props.visible} onDismiss={props.onCancel}>
          <CustomActivityIndicator
            style={styles.activityIndicator}
            visible={showIndicator}
          />
          <Dialog.Title>Change Access Rights</Dialog.Title>
          <Divider />
          <Dialog.Content>
            <Paragraph>{props.email}</Paragraph>
            <Paragraph>
              Admin: {props.admin.toString()}
            </Paragraph>
            <Paragraph>
              Accepted: {props.accepted.toString()}
            </Paragraph>
            <View>
            <CustomRadioButtonGroup
              default={value}
              values={values}
              sendDataToParent={getValue}
            />
            </View>
            <Divider />
            <View style={{ marginTop: 10 }}>
              <Button
                style={styles.resetButton}
                onPress={createResetAlert}
              >Reset Password</Button>
              {resetCode && <Text>Temporary password is: {resetCode}</Text>}
            </View>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={props.onCancel}>Cancel</Button>
            <Button 
              onPress={updateUser}
              disabled={
                !(
                  value != null
                )
              }
              >Done</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </View>
  );
};

export default CustomDialog;

const styles = StyleSheet.create({
  radioButton: {
    flexDirection: "row",
    alignItems: "center",
  },
  resetButton: {
    width: "70%",
  },
});
