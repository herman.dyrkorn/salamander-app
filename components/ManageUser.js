import React from "react";
import { StyleSheet, Text, View } from "react-native";

import CustomButton from "./CustomButton";
import CustomDialog from "./CustomDialog";
import { toastSuccess } from "../constants/toasts";

/**
 * This component renders a user with a "edit user" button, and is used in a list. The CustomDialog component is triggered when a user clicks
 * on this button. 
 * 
 * @param {String} props.email - the email of the user 
 * @param {Integer} props.id - the ID of the user 
 * @param {Boolean} props.admin - false/true if admin or not
 * @param {Boolean} props.accepted - false/true if the user is accepted or not
 * @param {Function} props.onCancel - dismisses the dialog
 * @param {Function} props.getUsers - to update the user-list after changes are submitted
 * @param {Function} props.showFeedback - triggers the toast message when changes are submitted
 * 
 * @returns the component. 
 */

const ManageUser = (props) => {
  const [visible, setVisible] = React.useState(false);

  const showDialog = () => setVisible(true);

  const hideDialog = () => setVisible(false);

  const hideDialogHandler = () => {
    hideDialog();
  };

  const showFeedback = (response) => {
    toastSuccess("bottom", 1000, response); 
  }

  return (
    <View style={styles.item}>
      <CustomDialog
        visible={visible}
        onCancel={hideDialogHandler}
        getUsers={props.getUsers}
        email={props.email}
        admin={props.admin}
        id={props.id}
        showFeedback={showFeedback}
        accepted={props.accepted}
      />
      <Text style={styles.title}>{props.email}</Text>
      <View style={styles.buttons}>
        <CustomButton
          style={styles.button}
          mode="contained"
          title="Edit"
          onPress={() => showDialog()}
        />
      </View>
    </View>
  );
};

export default ManageUser;

const styles = StyleSheet.create({
  item: {
    margin: 5,
    marginVertical: 8,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 16,
  },
  buttons: {
    flexDirection: "row",
    alignSelf: "flex-end",
  },
  title: {
    fontSize: 16,
  },
  button: {
    width: 80,
  },
});
