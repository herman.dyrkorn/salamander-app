import React from 'react'
import { StyleSheet, View, Modal, Text } from 'react-native'
import { Colors, ActivityIndicator } from "react-native-paper";

/**
 * This component is used in screens where there are newtwork requests. This can be done better in the
 * future, but its purpose is to prevent the user from aborting their task until they have received a response, 
 * and to prevent them from spamming a request. It is also used to visualize that the server is working. 
 * 
 * @param {Boolean} props.visible - this is the prop that makes the indicator visible or not. 
 * @param {Object} props.style - the style can be passed down to this component to customize it. 
 * @param {String} props.text - optional feature where a text can be passed to the component to
 *                              indicate that it is processing something. Currently only used when
 *                              image processing, since it takes some time. 
 * 
 * @returns the component
 */

const CustomActivityIndicator = (props) => {
    return (
        <Modal transparent={true} animationType="none" visible={props.visible}>
            <View
                style={{ ...styles.activityIndicator, ...props.style }}>
                <ActivityIndicator 
                    style={styles.loading} 
                    animating={props.visible} 
                    color={Colors.white}
                    size='large'
                />
                <Text style={styles.text}>
                    {props.text}
                </Text>
            </View>
        </Modal>
    )
}

export default CustomActivityIndicator

const styles = StyleSheet.create({
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
      }, 
    activityIndicator: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: `rgba(0,0,0,${0.1})`
    }, 
    text: {
        alignContent: "center", 
        marginTop: 100,
        color: 'white'
    }
})
