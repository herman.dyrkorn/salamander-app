import React, { useEffect } from "react";
import { View, Platform } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { IconButton } from "react-native-paper";

/**
 * This renders a component that makes the user able to upload an image from their own library. Expo image picker is
 * the library used, more documentation there 
 * 
 * @param {Function} props.setImageUri function that sets the uri for the image, sends it to the parent component. 
 * 
 * @returns the component
 */

const CustomImagePicker = (props) => {

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const {
          status,
        } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      quality: 1,
    });

    if (!result.cancelled) {
      props.setImageUri(result.uri);
    }
  };

  return (
    <View>
      <IconButton
        icon="image"
        color="#435768"
        size={90}
        onPress={pickImage}
      />
    </View>
  );
};

export default CustomImagePicker;
