import * as React from "react";
import { Appbar } from "react-native-paper";

/**
 * The header component that shows in top of the screens
 * 
 * @param {StackNavigator} navigation - keeps track of where we are in the stack, used to go back to the previous screen
 * @param {*} previous - checks if there are a previous screen in the stack, and renders a back button if there are
 * @param {*} scene - can extract the options and data of the navigation
 * 
 * @returns the component
 */

const CustomHeader = ({ navigation, previous, scene }) => {
  const { options } = scene.descriptor;
  const title =
    options.headerTitle !== undefined
      ? options.headerTitle
      : options.title !== undefined
      ? options.title
      : scene.route.name;

  return (
    <Appbar.Header>
      {previous ? <Appbar.BackAction onPress={navigation.goBack} /> : null}
      <Appbar.Content title={title} />
    </Appbar.Header>
  );
};

export default CustomHeader;
