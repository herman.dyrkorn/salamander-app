import React from "react";
import { StyleSheet } from "react-native";
import { Appbar } from "react-native-paper";

/**
 * This component is almost the same as the CustomHeader, except for not having the back-button option. 
 * These components can most probably be merged in a way. 
 * 
 * @param {*} scene - can extract the options and data of the navigation
 * 
 * @returns the component
 */

const EditUserHeader = ({ scene }) => {
  const { options } = scene.descriptor;
  const title =
    options.headerTitle !== undefined
      ? options.headerTitle
      : options.title !== undefined
      ? options.title
      : scene.route.name;

  return (
    <Appbar.Header style={styles.header}>
      <Appbar.Content title={title} />
    </Appbar.Header>
  );
};

export default EditUserHeader;

const styles = StyleSheet.create({
  header: {
    justifyContent: "center",
    flexDirection: "row",
  },
  image: {
    width: 40,
    height: 40,
    marginLeft: 15,
  },
});
