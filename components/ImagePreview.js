import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Modal,
} from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";

/**
 * This component displays an image through a modal, used for displaying the salamanders. 
 * This component can be refactored/improved in the future. 
 * 
 * @param {Function} props.setModalVisible - sets the modal visible
 * 
 * @returns the component
 */

export default function imagePreview(props) {
  const [image, setImage] = useState(null);

  const images = [
    {
      url: image,
    },
  ];

  useEffect(() => {
    setImage(props.img);
  }, []);

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={true}
      onRequestClose={() => {
        props.setModalVisible();
      }}
    >
      <View style={styles.previewContainer}>
        {image && (
          <ImageViewer
            onCancel={() => props.setModalVisible()}
            enableSwipeDown={true}
            imageUrls={images}
            renderIndicator={() => null}
          />
        )}
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  previewContainer: {
    flex: 1,
    backgroundColor: "black",
    height: "100%",
    width: "100%",
  },
  image: {
    flex: 1,
    height: undefined,
    width: undefined,
    resizeMode: "contain",
  },
});
