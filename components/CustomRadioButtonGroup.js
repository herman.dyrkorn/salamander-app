import React, { useState }  from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { RadioButton } from "react-native-paper";

/**
 * This component renders a customizable radiobutton group. It is used for sex selection (RegisterSalamanderScreen and
 * ChangeSalamanderScreen), and for managing users (CustomDialog component). 
 * 
 * @param {String} props.default - the default selection. Can be set, but is null in most cases. 
 * @param {Object} values - Contains six data entries, three for the labels and three for the values. 
 * @param {Function} sendDataToParent - a bit of a janky way to send data back to the mother component, 
 *                                      should consider to look into bind in the future. 
 * 
 * @returns the component. 
 */

const CustomRadioButtonGroup = (props) => {

    const [value, setValue] = useState(props.default);

    const callback = (item) => { 
        setValue(item);
        props.sendDataToParent(item);
      };

    return (
        <RadioButton.Group
            onValueChange={(newValue) => callback(newValue)}
            value={value}
            >
            <View style={styles.radioButton}>
                <RadioButton value={props.values.firstValue} />
                <Text>{props.values.firstText}</Text>
            </View>
            <View style={styles.radioButton}>
                <RadioButton value={props.values.secondValue} />
                <Text>{props.values.secondText}</Text>
            </View>
            <View style={styles.radioButton}>
                <RadioButton value={props.values.thirdValue} />
                <Text>{props.values.thirdText}</Text>
            </View>
        </RadioButton.Group>
    )
}

export default CustomRadioButtonGroup

const styles = StyleSheet.create({
    text: {
        marginStart: 20,
      },
      radioButton: {
        flexDirection: "row",
        alignItems: "center",
      },
})
