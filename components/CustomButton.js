import * as React from "react";
import { Button } from "react-native-paper";
import { StyleSheet } from "react-native";

/**
 * A customizable component, that renders a button styled by the react native paper library. 
 * 
 * @param {Object} props.style - (Optional) can override the default style of the button where necessary, like size and position
 * @param {String} props.color - (Optional) color of the button
 * @param {IconSource} props.icon - (Optional) Icon to display for the button
 * @param {String} props.mode - Mode/emphasis of the button. The modes outlined and contained have mostly been used.
 * @param {Function} props.onPress - function that is run when the button the pressed (gets passed down from the mother component in this case)
 * @param {Boolean} props.disabled - Whether the button is disabled. 
 * @param {String} props.title - The text on the button
 * 
 * @returns the component
 */

const CustomButton = (props) => (
  <Button
    style={{ ...styles.button, ...props.style }}
    color={props.color}
    icon={props.icon}
    mode={props.mode}
    color="#435768"
    onPress={props.onPress}
    disabled={props.disabled}
  >
    {props.title}
  </Button>
);

export default CustomButton;

const styles = StyleSheet.create({
  button: {
    width: "40%",
    alignSelf: "center",
    color: "#435768",
  },
});
