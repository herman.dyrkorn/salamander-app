import * as React from "react";
import { View, StyleSheet } from "react-native";
import { Button, Paragraph, Dialog, Portal, Text } from "react-native-paper";

/**
 * This is the component that displays the information after a user has tried to match a salamander.
 * It is a modal/portal, that appear when the user has tried to register a salamander. It is used in
 * both the RegisterSalamanderScreen and the ChangeSalamanderScreen (Whenever there a match process is performed)
 * 
 * @param {String} props.responseColor - background color. green color for match and white for no match
 * @param {Boolean} props.visible - if the component should be visible or not
 * @param {Function} props.onCancel - redirects the user to the next screen
 * @param {String} props.responseMatch - The title (Match/No match)
 * @param {String} props.responseText - The text received from the server
 * @param {Integer} props.id - the id of the salamander
 * 
 * @returns the component.
 */

const MatchResponse = (props) => {
  return (
    <View>
      <Portal>
        <Dialog
          style={{ backgroundColor: props.responseColor }}
          visible={props.visible}
          onDismiss={props.onCancel}
        >
          <Dialog.Title>{props.responseMatch}</Dialog.Title>
          <Dialog.Content>
            <Paragraph>
              {props.responseText} id: {props.id}
            </Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={props.onCancel}>
              <Text style={styles.button}>Done</Text>
            </Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </View>
  );
};

export default MatchResponse;

const styles = StyleSheet.create({
  dialog: {
    backgroundColor: "#fbbf5b",
  },
  button: {
    color: "black",
  },
});
