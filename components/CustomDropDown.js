import React, { useState } from "react";
import DropDownPicker from "react-native-dropdown-picker";

/**
 * The component that renders a dropdown menu. The react native dropdown picker library is used, more 
 * documentation on https://hossein-zare.github.io/react-native-dropdown-picker-website/ (looks like this doc is updated 
 * since we used it, might need to update the component to the latest version)
 * 
 * @param {String} props.defaultValue - the default value for the dropdown, in some cases null
 * @param {Array/List} props.myItems - list of items in the dropdown
 * @param {Function} props.sendDataToParent - a bit of a janky way to send data back to the mother component, 
 *                                            should consider to look into bind in the future (bind?). 
 * 
 * @returns the component
 */

const CustomDropDown = (props) => {
  const [value, setValue] = useState(props.defaultValue);
  const [items, setItems] = useState(props.myItems);
  let controller;

  const callback = (item) => {
    (item) => setValue(item.value);
    props.sendDataToParent(item.value);
  };

  return (
      <DropDownPicker
        items={items}
        controller={(instance) => (controller = instance)}
        onChangeList={(items, callback) => {
          new Promise((resolve, reject) => resolve(setItems(items)))
            .then(() => callback())
            .catch(() => {});
        }}
        defaultValue={value}
        onChangeItem={(item) => callback(item)}
        style={{ backgroundColor: "#fafafa" }}
        dropDownStyle={{ backgroundColor: "#fafafa" }}
        containerStyle={{ height: 40 }}
        itemStyle={{ justifyContent: "flex-start" }}
      />
  );
};

export default CustomDropDown;
