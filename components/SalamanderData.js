import React from "react";
import { StyleSheet, Text } from "react-native";
import { Card, Paragraph } from "react-native-paper";

/**
 * This components shows salamader data in a card in the SalamanderOverviewScreen. 
 * 
 * @param {Integer} props.id - the ID of the salamander 
 * @param {String} props.location - the location of the salamander 
 * @param {String} props.sex - the sex of the salamander
 * @param {String} props.species - the species of the salamander
 * 
 * @returns the component
 */

const SalamanderData = (props) => {
  return (
    <Card style={styles.card}>
      <Card.Title title="Salamander Data" />
      <Card.Content>
        <Paragraph>
          <Text style={styles.text}>ID: </Text>
          {props.id}
        </Paragraph>
        <Paragraph>
          <Text style={styles.text}>Location: </Text>
          {props.location}
        </Paragraph>
        <Paragraph>
          <Text style={styles.text}>Sex: </Text>
          {props.sex}{" "}
        </Paragraph>
        <Paragraph>
          <Text style={styles.text}>Species: </Text>
          {props.species}
        </Paragraph>
      </Card.Content>
    </Card>
  );
};

export default SalamanderData;

const styles = StyleSheet.create({
  card: {
    marginStart: 20, 
    marginEnd: 20
  },
  text: {
    fontWeight: "bold",
  }
});
