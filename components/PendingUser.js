import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { IconButton, Colors } from "react-native-paper";

import APIKit from "../APIkit";
import { toast500, toastError, toastSuccess } from "../constants/toasts";

/**
 * The component that renders the users in the PendingUsersScreen. Consists of the users email, 
 * a button for accepting the user and a button for denying access to the user. 
 * 
 * @param {Function} props.getPendingUsers - updates the list after a user has been accepted/denied
 * @param {String} props.email - the email of the user
 * @param {Integer} props.id - the ID of the user 
 *  
 * @returns the component
 */

const PendingUser = (props) => {
  const acceptUser = (email, id) => {
    let bodyFormData = new FormData();
    bodyFormData.append("id", id);
    bodyFormData.append("email", email);
    bodyFormData.append("accepted", 1);

    APIKit.post("/pendingUsers", bodyFormData)
      .then(function (response) {
        if (response.data.status === 200) {
          toastSuccess("bottom", 1000, response.data.message); 
          props.getPendingUsers();
        } else {
          toastError("bottom", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        toast500(response.status); 
      });
  };

  const denyUser = (email, id) => {
    let bodyFormData = new FormData();
    bodyFormData.append("id", id);
    bodyFormData.append("email", email);
    bodyFormData.append("accepted", 0);

    APIKit.post("/pendingUsers", bodyFormData)
      .then(function (response) {
        if (response.data.status === 200) {
          toastSuccess("bottom", 1000, response.data.message); 
          props.getPendingUsers();
        } else {
          toastError("bottom", 3000, response.data.message); 
        }
      })
      .catch(function (response) {
        toast500(response.status); 
      });
  };

  return (
    <View style={styles.item}>
      <Text style={styles.title}>{props.email}</Text>
      <View style={styles.buttons}>
        <IconButton
          icon="cancel"
          color={Colors.red700}
          size={30}
          onPress={() => {
            denyUser(props.email, props.id);
          }}
        />
        <IconButton
          icon="check-circle-outline"
          color={Colors.green800}
          size={30}
          onPress={() => {
            acceptUser(props.email, props.id);
          }}
        />
      </View>
    </View>
  );
};

export default PendingUser;

const styles = StyleSheet.create({
  item: {
    margin: 5,
    marginVertical: 8,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 16,
  },
  buttons: {
    flexDirection: "row",
    alignSelf: "flex-end",
  },
  title: {
    fontSize: 16,
  },
});
