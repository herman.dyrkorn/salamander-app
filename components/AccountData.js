import React from "react";
import { StyleSheet, Text, View } from "react-native";

/**
 * This component simply shows a user's data. This is used in the ChangeNameScreen component and
 * the ChangeEmailScreen component. 
 * 
 * @param {String} props.currentLabel - bolded text to show label for current <something>
 * @param {String} props.current - text to show current data
 * @param {String} props.newLabel - text to show label for new <something>
 * 
 * @returns the component
 */

const AccountData = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{props.currentLabel}</Text>
      <Text style={styles.data}>{props.current}</Text>
      <Text style={styles.title}>{props.newLabel}</Text>
    </View>
  );
};

export default AccountData;

const styles = StyleSheet.create({
  title: {
    fontWeight: "bold",
    fontSize: 14,
    marginTop: 5,
  },
  data: {
    fontSize: 16,
    marginTop: 5,
  },
  container: {
    marginStart: 30,
    marginTop: 20,
  },
});
