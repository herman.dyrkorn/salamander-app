import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements";

/**
 * This component renders the style used in the ProfileScreen, and makes each entry have the same touchable opacity look. 
 * 
 * @param {Function} props.onPress - function that is called when the component is clicked on. 
 * @param {String} props.title - the text that is visible on the component. 
 * @param {Object} props.style - (OPTIONAL) customizable style for the component. 
 * @param {IconSource} props.icon - (OPTIONAL) displays an icon on the component. 
 * 
 * @returns the component. 
 */

const CustomTouchableOpacity = (props) => {
  return (
    <TouchableOpacity style={styles.touchable} onPress={props.onPress}>
      <View style={styles.touchableText}>
        <Text style={{ fontWeight: "bold" }}>{props.title}</Text>
        <Text style={{ ...styles.placeholderText, ...props.style }}>
          {props.placeholder}
        </Text>
        <Icon name={props.icon} />
      </View>
    </TouchableOpacity>
  );
};

export default CustomTouchableOpacity;

const styles = StyleSheet.create({
  touchable: {
    alignSelf: "stretch",
    backgroundColor: "#eaeaea",
    padding: 10,
  },
  touchableText: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  placeholderText: {
    color: "#727273",
    marginLeft: "auto",
    paddingEnd: 5,
  },
});
