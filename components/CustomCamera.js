import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Modal,
} from "react-native";
import { Camera } from "expo-camera";
import { IconButton } from "react-native-paper";

/**
 * This component renders the built-in camera in the application. It is from the expo-camera library. 
 * 
 * @param {Function} props.deleteImage - deletes image from cache
 * @param {Function} props.setImageUri - passes the uri to the cameraScreen
 * @param {Function} props.setModalVisible - displays the modal with the camera
 * @param {Boolean} props.premission - If you have access to the camera or not
 * 
 * @returns the component
 */

export default function customCamera(props) {
  const [camera, setCamera] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);

  const takePicture = async () => {
    if (camera) {
      const data = await camera.takePictureAsync(null);
      props.deleteImage();
      props.setImageUri(data.uri);
      props.setModalVisible();
    }
  };

  if (props.permission === null) {
    return <Text>null!</Text>;
  }

  if (props.permission === false) {
    return <Text>No access to camera</Text>;
  }

  if (props.permission) {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={true}
        onRequestClose={() => {
          props.setModalVisible();
        }}
      >
        <View style={{ flex: 1, backgroundColor: "black" }}>
          <View style={styles.cameraContainer}>
            <Camera
              ref={(ref) => setCamera(ref)}
              style={styles.fixedRatio}
              type={type}
              ratio={"4:3"}
            >
              <View style={styles.buttonContainer}>
                <IconButton
                  style={styles.button}
                  icon="close"
                  color="white"
                  size={35}
                  onPress={() => {
                    props.setModalVisible();
                  }}
                />
                <IconButton
                  style={styles.button}
                  icon="circle-outline"
                  color="white"
                  size={50}
                  onPress={() => {
                    takePicture();
                  }}
                />
                <IconButton
                  style={styles.button}
                  icon="axis-z-rotate-clockwise"
                  color="white"
                  size={35}
                  onPress={() => {
                    setType(
                      type === Camera.Constants.Type.back
                        ? Camera.Constants.Type.front
                        : Camera.Constants.Type.back
                    );
                  }}
                />
              </View>
            </Camera>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  cameraContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  fixedRatio: {
    flex: 1,
    aspectRatio: 0.75,
  },
  buttonContainer: {
    flex: 1,
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "center",
  },
  button: {
    flex: 1,
  },
});
