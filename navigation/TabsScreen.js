import React from "react";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import { HomeScreen } from "../screens/home/HomeScreen";
import { CameraScreen } from "../screens/camera/CameraScreen";
import { ProfileScreen } from "../screens/profile/ProfileScreen";
import CustomHeader from "../components/CustomHeader";
import { ChangeNameScreen } from "../screens/profile/ChangeNameScreen";

const Tabs = createMaterialBottomTabNavigator();
const HomeStack = createStackNavigator();
const CameraStack = createStackNavigator();
const ProfileStack = createStackNavigator();

/**
 * The TabsScreen consists of three stack navigator screens, home, camera and profile. 
 * 
 * @returns the component
 */

const ProfileStackScreen = () => (
  <ProfileStack.Navigator
    initialRouteName="Profile"
    screenOptions={{
      header: (props) => <CustomHeader {...props} />,
    }}
  >
    <ProfileStack.Screen
      name="Profile"
      component={ProfileScreen}
      options={{ headerTitle: "Profile" }}
    />
    <ProfileStack.Screen
      name="ChangeName"
      component={ChangeNameScreen}
      options={{ headerTitle: "Change Name" }}
    />
  </ProfileStack.Navigator>
);

const CameraStackScreen = () => (
  <CameraStack.Navigator
    initialRouteName="Camera"
    screenOptions={{
      header: (props) => <CustomHeader {...props} />,
    }}
  >
    <CameraStack.Screen
      name="Camera"
      component={CameraScreen}
      options={{ headerTitle: "Camera" }}
    />
  </CameraStack.Navigator>
);

const HomeStackScreen = () => (
  <HomeStack.Navigator
    initialRouteName="Home"
    screenOptions={{
      header: (props) => <CustomHeader {...props} />,
    }}
  >
    <HomeStack.Screen
      name="Home"
      component={HomeScreen}
      options={{ headerTitle: "Home" }}
    />
  </HomeStack.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator
    initialRouteName="Home"
    activeColor="#f6d9a8"
    barStyle={{ backgroundColor: "orange" }}
  >
    <Tabs.Screen
      name="Home"
      component={HomeStackScreen}
      options={{
        tabBarLabel: "Home",
        tabBarIcon: ({ color }) => (
          <MaterialCommunityIcons name="home" color={color} size={26} />
        ),
      }}
    />
    <Tabs.Screen
      name="Camera"
      component={CameraStackScreen}
      options={{
        tabBarLabel: "Camera",
        tabBarIcon: ({ color }) => (
          <MaterialCommunityIcons name="camera" color={color} size={26} />
        ),
      }}
    />
    <Tabs.Screen
      name="Profile"
      component={ProfileStackScreen}
      options={{
        tabBarLabel: "Profile",
        tabBarIcon: ({ color }) => (
          <MaterialCommunityIcons name="account" color={color} size={26} />
        ),
      }}
    />
  </Tabs.Navigator>
);

export default TabsScreen;
