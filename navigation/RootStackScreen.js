import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import TabsScreen from "./TabsScreen";
import AuthStackScreen from "./AuthStackScreen";
import ModalStackScreen from "./ModalStackScreen";

const RootStack = createStackNavigator();

/**
 * A stack navigator that contains the three navigator screens TabsScreen, AuthstackScreen and ModalStackScreen.
 * 
 * @param {String} usertoken - will display the app screens if there are a usertoken, displays the authstack screens if there are none. 
 * 
 * @returns the component
 */
const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode="none" mode="modal">
    {userToken ? (
      <RootStack.Screen
        name="App"
        component={TabsScreen}
        options={{
          animationEnabled: false,
        }}
      />
    ) : (
      <RootStack.Screen
        name="Auth"
        component={AuthStackScreen}
        options={{
          animationEnabled: false,
        }}
      />
    )}
    <RootStack.Screen name="Verify" component={ModalStackScreen} />
  </RootStack.Navigator>
);

export default RootStackScreen;
