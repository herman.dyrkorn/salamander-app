import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { SignInScreen } from "../screens/SignInScreen";
import SignUpScreen from "../screens/SignUpScreen";

const AuthStack = createStackNavigator();

/**
 * A stack navigator that contains the authentication screens. 
 * 
 * @returns the component
 */
const AuthStackScreen = () => (
  <AuthStack.Navigator
    screenOptions={{
      headerShown: false,
    }}
  >
    <AuthStack.Screen
      name="SignIn"
      component={SignInScreen}
      options={{
        animationEnabled: false,
      }}
    />
    <AuthStack.Screen
      name="SignUp"
      component={SignUpScreen}
      options={{
        animationEnabled: false,
      }}
    />
  </AuthStack.Navigator>
);

export default AuthStackScreen;
