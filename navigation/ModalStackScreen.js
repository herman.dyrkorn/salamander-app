import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import VerifyPasswordScreen from "../screens/VerifyPasswordScreen";
import { ChangeEmailScreen } from "../screens/profile/ChangeEmailScreen";
import ChangePasswordScreen from "../screens/profile/ChangePasswordScreen";
import { DeleteAccountScreen } from "../screens/profile/DeleteAccountScreen";
import ManageUsersScreen from "../screens/admin/ManageUsersScreen";
import PendingUsersScreen from "../screens/admin/PendingUsersScreen";
import EditUserHeader from "../components/EditUserHeader";
import { RegisterLocationScreen } from "../screens/home/RegisterLocationScreen";
import { RegisterSalamanderScreen } from "../screens/camera/RegisterSalamanderScreen";
import { ManageSalamandersScreen } from "../screens/admin/ManageSalamandersScreen";
import { ChangeSalamanderScreen } from "../screens/admin/ChangeSalamanderScreen";
import SalamanderOverviewScreen from "../screens/admin/SalamanderOverviewScreen";
import { EditLocationScreen } from "../screens/home/EditLocationScreen";

const ModalStack = createStackNavigator();

/**
 * A stack navigator which contains all screens that is a modal. It if fairly large, and could be organized in a better way?
 * 
 * @returns the component
 */
const ModalStackScreen = () => (
  <ModalStack.Navigator
    screenOptions={{
      header: (props) => <EditUserHeader {...props} />,
    }}
  >
    <ModalStack.Screen
      name="VerifyPassword"
      component={VerifyPasswordScreen}
      options={{ headerTitle: "Verify Password" }}
    />
    <ModalStack.Screen
      name="ChangeEmail"
      component={ChangeEmailScreen}
      options={{ headerTitle: "Change Email" }}
    />
    <ModalStack.Screen
      name="ChangePassword"
      component={ChangePasswordScreen}
      options={{ headerTitle: "Change Password" }}
    />
    <ModalStack.Screen
      name="PendingUsers"
      component={PendingUsersScreen}
      options={{ headerTitle: "Pending Users" }}
    />
    <ModalStack.Screen
      name="ManageUsers"
      component={ManageUsersScreen}
      options={{ headerTitle: "Manage Users" }}
    />
    <ModalStack.Screen
      name="DeleteAccount"
      component={DeleteAccountScreen}
      options={{ headerTitle: "Delete Account" }}
    />
    <ModalStack.Screen
      name="RegisterLocation"
      component={RegisterLocationScreen}
      options={{ headerTitle: "Register Location" }}
    />
    <ModalStack.Screen
      name="RegisterSalamander"
      component={RegisterSalamanderScreen}
      options={{ headerTitle: "Register Salamander" }}
    />
    <ModalStack.Screen
      name="ManageSalamanders"
      component={ManageSalamandersScreen}
      options={{ headerTitle: "Manage Salamanders" }}
    />
    <ModalStack.Screen
      name="ChangeSalamander"
      component={ChangeSalamanderScreen}
      options={{ headerTitle: "Change Salamander Data" }}
    />
    <ModalStack.Screen
      name="SalamanderOverview"
      component={SalamanderOverviewScreen}
      options={{ headerTitle: "Salamander Overview" }}
    />
    <ModalStack.Screen
      name="EditLocation"
      component={EditLocationScreen}
      options={{ headerTitle: "Edit Location" }}
    />
  </ModalStack.Navigator>
);

export default ModalStackScreen;
